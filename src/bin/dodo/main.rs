// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019-2021 Sven Stegemann

extern crate clap;
extern crate dodo;

use std::io::{stdin, stdout};

use clap::{App, Arg, SubCommand};

use dodo::built_info;
use dodo::parsing::FenConvertable;
use dodo::representation::GameState;
use dodo::search::perft;
use dodo::uci::Engine;

fn main() {
    let matches = App::new("Dodo")
        .version(built_info::PKG_VERSION)
        .author(built_info::PKG_AUTHORS)
        .about(built_info::PKG_DESCRIPTION)
        .subcommand(
            SubCommand::with_name("perft")
                .about("Returns the number of positions reachable after exactly DEPTH plies.")
                .arg(Arg::with_name("DEPTH").required(true).index(1))
                .arg(Arg::with_name("FEN").required(false).index(2))
                .arg(Arg::with_name("MOVES").required(false).index(3)),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("perft") {
        let depth: i32 = matches.value_of("DEPTH").unwrap().parse().unwrap();
        let mut game_state = if let Some(fen) = matches.value_of("FEN") {
            GameState::from_fen(fen).expect("FEN must be valid.")
        } else {
            GameState::get_starting_position()
        };
        for move_ in matches
            .value_of("MOVES")
            .unwrap_or_default()
            .split(' ')
            .filter(|s| !s.is_empty())
            .map(|m| m.try_into().unwrap())
        {
            game_state.make_move_unchecked(move_);
        }
        println!("\n{}", perft(game_state, depth, true));

        return;
    }

    Engine::run(stdin(), stdout());
}
