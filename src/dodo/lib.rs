// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019-2021 Sven Stegemann

#![allow(incomplete_features)]
#![feature(associated_type_bounds)]
#![feature(try_trait_v2)]
#![feature(generic_const_exprs)]
#![feature(const_trait_impl)]
#![feature(specialization)]
#![feature(let_chains)]
#![feature(box_syntax)]
#![feature(exclusive_range_pattern)]
#![feature(trait_alias)]

#[macro_use]
extern crate lazy_static;

pub mod evaluation;
pub mod parsing;
pub mod representation;
pub mod search;
pub mod uci;
pub mod util;

pub mod built_info {
    // The file has been placed there by the build script.
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}
