// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019-2021 Sven Stegemann

pub mod attacks;
pub mod move_generation;
pub mod perft;
pub mod search_options;
pub mod searcher;
pub mod transposition_table;

pub use attacks::Attacks;
pub use move_generation::MoveGenerator;
pub use perft::perft;
pub use search_options::{ClockState, ClockStateForSide, SearchLimit, SearchOptions};
pub use searcher::{SearchFinishedEvent, SearchManager, SearchUpdateEvent};
pub use transposition_table::{TranspositionTable, TranspositionTableEntry};
