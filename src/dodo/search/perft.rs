// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019-2021 Sven Stegemann

use crate::representation::GameState;
use crate::search::{Attacks, MoveGenerator};

/// Counts the number of *leaf* nodes reachable from the given `GameState` in at most `depth` ply.
/// Perft is usually used to validate the correctedness of some parts of an chess engine like the move generator.
/// See: <https://www.chessprogramming.org/Perft>
///
/// If `divide` is true perft results for every move from the root node will be printed.
pub fn perft(game_state: GameState, depth: i32, divide: bool) -> i64 {
    if depth == 0 {
        return 1;
    }

    let mut number_of_nodes = 0;

    let move_list = game_state.pseudolegal_moves();

    for move_ in move_list {
        let mut new_game_state = game_state.clone();
        new_game_state.make_move_unchecked(move_);
        if new_game_state.king_can_be_captured() {
            continue;
        }
        let inner_perft = perft(new_game_state, depth - 1, false);
        number_of_nodes += inner_perft;
        if divide {
            println!("{move_} {inner_perft}");
        }
    }

    number_of_nodes
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsing::FenConvertable;

    fn test_perft_fen(fen: &str, depth: i32, expected_perft: i64) {
        assert_eq!(
            perft(GameState::from_fen(fen).unwrap(), depth, false),
            expected_perft
        );
    }

    fn test_perft_starting_position(depth: i32, expected_perft: i64) {
        assert_eq!(
            perft(GameState::get_starting_position(), depth, false),
            expected_perft
        );
    }

    // The following positions and perft results are taken from https://www.chessprogramming.org/Perft_Results.

    #[test]
    fn perft_of_starting_position_depth_1() {
        test_perft_starting_position(1, 20)
    }

    #[test]
    fn perft_of_starting_position_depth_2() {
        test_perft_starting_position(2, 400)
    }

    #[test]
    fn perft_of_starting_position_depth_3() {
        test_perft_starting_position(3, 8_902)
    }

    #[test]
    fn perft_of_starting_position_depth_4() {
        test_perft_starting_position(4, 197_281)
    }

    #[test]
    #[ignore]
    fn perft_of_starting_position_depth_5() {
        test_perft_starting_position(5, 4_865_609)
    }

    #[test]
    #[ignore]
    fn perft_of_starting_position_depth_6() {
        test_perft_starting_position(6, 119_060_324)
    }

    #[test]
    fn perft_of_chessprogramming_position_2_depth1() {
        test_perft_fen(
            "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 1 1",
            1,
            48,
        )
    }

    #[test]
    fn perft_of_chessprogramming_position_2_depth2() {
        test_perft_fen(
            "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 1 1",
            2,
            2_039,
        )
    }

    #[test]
    fn perft_of_chessprogramming_position_2_depth3() {
        test_perft_fen(
            "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 1 1",
            3,
            97_862,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_2_depth4() {
        test_perft_fen(
            "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 1 1",
            4,
            4_085_603,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_2_depth5() {
        test_perft_fen(
            "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 1 1",
            5,
            193_690_690,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_3_depth4() {
        test_perft_fen("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 1 1", 4, 43_238)
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_3_depth5() {
        test_perft_fen("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 1 1", 5, 674_624)
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_4_depth4() {
        test_perft_fen(
            "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
            4,
            422_333,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_4_depth5() {
        test_perft_fen(
            "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
            5,
            15_833_292,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_5_depth4() {
        test_perft_fen(
            "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
            4,
            2_103_487,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_5_depth5() {
        test_perft_fen(
            "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
            5,
            89_941_194,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_6_depth4() {
        test_perft_fen(
            "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10",
            4,
            3_894_594,
        )
    }

    #[test]
    #[ignore]
    fn perft_of_chessprogramming_position_6_depth5() {
        test_perft_fen(
            "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10 ",
            5,
            164_075_551,
        )
    }
}
