// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019-2021 Sven Stegemann

extern crate derive_more;
extern crate strum;

use strum::IntoEnumIterator;

use crate::representation::{
    CastlingType, File, GameState, Move, MoveSet, Piece, PieceKind, Promotion, Rank, Square, Vector,
};
use crate::search::Attacks;

/// Generate every pseudolegal move except for pawn and castling moves.
fn add_pseudolegal_piece_moves(move_list: &mut MoveSet, game_state: &GameState) {
    let side_to_move = game_state.get_side_to_move();
    let occupied_by_side_to_move = game_state.get_board().get_occupied_by_color(side_to_move);
    let pieces = PieceKind::iter()
        .filter(|piece_kind| *piece_kind != PieceKind::PAWN)
        .map(|piece_kind| Piece::new(game_state.get_side_to_move(), piece_kind));

    for piece in pieces {
        for origin_square in game_state.get_board().get_positions(piece) {
            for destination_square in
                game_state.attacks_from_square(origin_square) & !occupied_by_side_to_move
            {
                move_list.add_move(Move {
                    from: origin_square,
                    to: destination_square,
                    promotion: Promotion::NONE,
                })
            }
        }
    }
}

fn add_pseudolegal_pawn_moves(move_list: &mut MoveSet, game_state: &GameState) {
    let side_to_move = game_state.get_side_to_move();
    let board = game_state.get_board();
    let advance_vector = Vector::get_relative_to_side(side_to_move, Vector::NORTH);
    let occupied = board.get_occupied();
    let occupied_by_opponent = board.get_occupied_by_color(side_to_move.get_opponent());
    let pawn_squares = board
        .get_positions(Piece::new(side_to_move, PieceKind::PAWN))
        .into_iter();
    let en_passant_square = game_state.get_en_passant_square();

    for origin_square in pawn_squares {
        let possible_promotions =
            if origin_square.get_rank() == Rank::get_relative_to_side(side_to_move, Rank::_7) {
                vec![
                    Promotion::QUEEN,
                    Promotion::KNIGHT,
                    Promotion::ROOK,
                    Promotion::BISHOP,
                ]
            } else {
                vec![Promotion::NONE]
            };

        if !occupied.is_set((origin_square + advance_vector).unwrap()) {
            // Generate single pawn advance.
            for promotion in &possible_promotions {
                move_list.add_move(Move {
                    from: origin_square,
                    to: (origin_square + advance_vector).unwrap(),
                    promotion: *promotion,
                })
            }

            // Generate double pawn advance.
            if origin_square.get_rank() == Rank::get_relative_to_side(side_to_move, Rank::_2)
                && !occupied.is_set((origin_square + 2 * advance_vector).unwrap())
            {
                move_list.add_move(Move {
                    from: origin_square,
                    to: (origin_square + 2 * advance_vector).unwrap(),
                    promotion: Promotion::NONE,
                })
            }
        }

        // Generate captures.
        let mut capture_destinations = occupied_by_opponent;
        if let Some(en_passant_square) = en_passant_square {
            capture_destinations.set(en_passant_square);
        }
        capture_destinations &= game_state.attacks_from_square(origin_square);
        for destination_square in capture_destinations {
            for promotion in &possible_promotions {
                move_list.add_move(Move {
                    from: origin_square,
                    to: destination_square,
                    promotion: *promotion,
                })
            }
        }
    }
}

fn add_pseudolegal_castling_moves(move_list: &mut MoveSet, game_state: &GameState) {
    let side_to_move = game_state.get_side_to_move();
    let board = game_state.get_board();
    let castling_rank = Rank::get_relative_to_side(game_state.get_side_to_move(), Rank::_1);
    let king_square = Square::new(File::E, castling_rank);
    let attacked_squares = game_state.attacks(side_to_move.get_opponent()); // Can't move over squares the opponent attacks.

    if game_state.king_is_in_check() {
        return;
    }

    for castling_type in CastlingType::iter() {
        if !game_state.can_castle_to_side(side_to_move, castling_type) {
            continue;
        }

        let rook_square = match castling_type {
            CastlingType::QUEENSIDE => Square::new(File::A, castling_rank),
            CastlingType::KINGSIDE => Square::new(File::H, castling_rank),
        };

        debug_assert_eq!(
            board.get_piece_on_square(king_square),
            Some(Piece::new(side_to_move, PieceKind::KING))
        );
        debug_assert_eq!(
            board.get_piece_on_square(rook_square),
            Some(Piece::new(side_to_move, PieceKind::ROOK))
        );

        // Ensure every square between king_square and rook_square is empty.
        let files_between = match castling_type {
            CastlingType::QUEENSIDE => vec![File::B, File::C, File::D],
            CastlingType::KINGSIDE => vec![File::F, File::G],
        };
        if files_between
            .iter()
            .map(|file| Square::new(*file, castling_rank))
            .any(|square| board.get_occupied().is_set(square))
        {
            continue;
        }

        // Ensure the files the king jumps over and lands on are not attacked.
        let must_not_be_attacked = match castling_type {
            CastlingType::QUEENSIDE => vec![File::C, File::D],
            CastlingType::KINGSIDE => vec![File::F, File::G],
        };
        if must_not_be_attacked
            .iter()
            .map(|file| Square::new(*file, castling_rank))
            .any(|square| attacked_squares.is_set(square))
        {
            continue;
        }

        // Castling moves are encoded as "king moves two fields".
        move_list.add_move(Move {
            from: king_square,
            to: (king_square
                + 2 * (match castling_type {
                    CastlingType::QUEENSIDE => Vector::WEST,
                    CastlingType::KINGSIDE => Vector::EAST,
                }))
            .unwrap(),
            promotion: Promotion::NONE,
        });
    }
}

pub trait MoveGenerator {
    fn pseudolegal_moves(&self) -> MoveSet;
    fn legal_moves(&self) -> MoveSet;

    fn is_pseudolegal_move(&self, move_: Move) -> bool;
    fn is_legal_move(&self, move_: Move) -> bool;
    fn pseudolegal_move_is_legal(&self, pseudolegal_move: Move) -> bool;
}

impl MoveGenerator for GameState {
    fn pseudolegal_moves(&self) -> MoveSet {
        let mut result = MoveSet::default();

        add_pseudolegal_piece_moves(&mut result, self);
        add_pseudolegal_pawn_moves(&mut result, self);
        add_pseudolegal_castling_moves(&mut result, self);

        result
    }

    fn legal_moves(&self) -> MoveSet {
        MoveSet::from_vec(
            self.pseudolegal_moves()
                .into_iter()
                .filter(|pseudolegal_move| self.pseudolegal_move_is_legal(*pseudolegal_move))
                .collect(),
        )
    }

    fn is_pseudolegal_move(&self, move_: Move) -> bool {
        self.pseudolegal_moves().contains(move_)
    }

    fn is_legal_move(&self, move_: Move) -> bool {
        self.is_pseudolegal_move(move_) && self.pseudolegal_move_is_legal(move_)
    }

    fn pseudolegal_move_is_legal(&self, pseudolegal_move: Move) -> bool {
        let mut next_state = self.clone();
        next_state.make_move_unchecked(pseudolegal_move);
        !next_state.king_can_be_captured()
    }
}
