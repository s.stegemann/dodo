// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

use std::time::{Duration, Instant};

use enum_map::EnumMap;

use crate::representation::{Color, MoveSet};

/// The options for a search. This struct is based on the "go" command of the UCI spec.
#[derive(Default, Debug, Eq, PartialEq, Clone)]
pub struct SearchOptions {
    pub search_moves: Option<MoveSet>,
    pub ponder: bool,
    pub search_limits: Vec<SearchLimit>,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
#[derive(Default)]
pub enum SearchLimit {
    Clock(ClockState), //< Use time management to determine search time for this move.
    Depth(i32),        //< Search to a certain depth.
    Nodes(i64),        //< Search a certain number of nodes.
    Mate(i32),         //< Search for mate in x moves.
    Time(Instant),     //< Search up to a certain point in time.
    #[default]
    Infinite,          //< Search until stop command is received.
}



#[derive(Default, Debug, Eq, PartialEq, Copy, Clone)]
pub struct ClockState {
    pub clocks: EnumMap<Color, Option<ClockStateForSide>>,
    pub moves_to_go: Option<i32>, //< Number of moves until the next clock period starts.
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct ClockStateForSide {
    pub clock_ends: Instant,
    pub increment: Duration,
}
