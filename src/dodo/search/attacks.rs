// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019-2021 Sven Stegemann

use crate::representation::{Bitboard, Color, GameState, Piece, PieceKind, Square, Vector};
use strum::IntoEnumIterator;

/// Generates attacks for kings, knights and pawns.
fn jumper_attacks(origin_square: Square, directions: Vec<Vector>) -> Bitboard {
    let mut result = Bitboard::EMPTY;

    for target_square in directions
        .iter()
        .map(|direction| origin_square + *direction)
        .filter_map(|square| square.ok())
    {
        result.set(target_square)
    }

    result
}

/// Generates attacks for rooks, bishops and queens.
fn slider_attacks(origin_square: Square, occupancy: Bitboard, directions: Vec<Vector>) -> Bitboard {
    let mut result = Bitboard::EMPTY;

    for direction in directions.iter() {
        let mut next_square = origin_square + *direction;
        while let Ok(target_square) = next_square {
            result.set(target_square);
            if occupancy.is_set(target_square) {
                break;
            }
            next_square = target_square + *direction;
        }
    }

    result
}

fn pawn_attacks(origin_square: Square, color: Color) -> Bitboard {
    jumper_attacks(
        origin_square,
        vec![
            Vector::get_relative_to_side(color, Vector::NORTHEAST),
            Vector::get_relative_to_side(color, Vector::NORTHWEST),
        ],
    )
}

fn knight_attacks(origin_square: Square) -> Bitboard {
    jumper_attacks(
        origin_square,
        vec![
            Vector::NORTH + Vector::NORTHWEST,
            Vector::NORTH + Vector::NORTHEAST,
            Vector::WEST + Vector::NORTHWEST,
            Vector::WEST + Vector::SOUTHWEST,
            Vector::SOUTH + Vector::SOUTHWEST,
            Vector::SOUTH + Vector::SOUTHEAST,
            Vector::EAST + Vector::NORTHEAST,
            Vector::EAST + Vector::SOUTHEAST,
        ],
    )
}

fn king_attacks(origin_square: Square) -> Bitboard {
    jumper_attacks(
        origin_square,
        vec![
            Vector::NORTH,
            Vector::NORTHWEST,
            Vector::WEST,
            Vector::SOUTHWEST,
            Vector::SOUTH,
            Vector::SOUTHEAST,
            Vector::EAST,
            Vector::NORTHEAST,
        ],
    )
}

fn bishop_attacks(origin_square: Square, occupancy: Bitboard) -> Bitboard {
    slider_attacks(
        origin_square,
        occupancy,
        vec![
            Vector::NORTHEAST,
            Vector::NORTHWEST,
            Vector::SOUTHEAST,
            Vector::SOUTHWEST,
        ],
    )
}

fn rook_attacks(origin_square: Square, occupancy: Bitboard) -> Bitboard {
    slider_attacks(
        origin_square,
        occupancy,
        vec![Vector::NORTH, Vector::WEST, Vector::SOUTH, Vector::EAST],
    )
}

fn queen_attacks(origin_square: Square, occupancy: Bitboard) -> Bitboard {
    bishop_attacks(origin_square, occupancy) | rook_attacks(origin_square, occupancy)
}

fn attacks_with(piece: Piece, origin_square: Square, occupancy: Bitboard) -> Bitboard {
    match piece.kind {
        PieceKind::PAWN => pawn_attacks(origin_square, piece.color),
        PieceKind::KNIGHT => knight_attacks(origin_square),
        PieceKind::KING => king_attacks(origin_square),
        PieceKind::BISHOP => bishop_attacks(origin_square, occupancy),
        PieceKind::ROOK => rook_attacks(origin_square, occupancy),
        PieceKind::QUEEN => queen_attacks(origin_square, occupancy),
    }
}

pub trait Attacks {
    /// Returns a `Bitboard` of every square that `color` attacks.
    fn attacks(&self, color: Color) -> Bitboard;
    fn attacks_with_piece(&self, piece: Piece) -> Bitboard;
    fn attacks_from_square(&self, square: Square) -> Bitboard;

    fn king_is_in_check(&self) -> bool;
    fn king_can_be_captured(&self) -> bool;
}

impl Attacks for GameState {
    fn attacks(&self, color: Color) -> Bitboard {
        PieceKind::iter()
            .map(|piece_kind| self.attacks_with_piece(Piece::new(color, piece_kind)))
            .fold(Bitboard::EMPTY, |a, b| a | b)
    }

    fn attacks_with_piece(&self, piece: Piece) -> Bitboard {
        self.get_board()
            .get_positions(piece)
            .into_iter()
            .map(|origin_square| {
                attacks_with(piece, origin_square, self.get_board().get_occupied())
            })
            .fold(Bitboard::EMPTY, |a, b| a | b)
    }

    fn attacks_from_square(&self, origin_square: Square) -> Bitboard {
        let piece = self.get_board().get_piece_on_square(origin_square);

        if let Some(piece) = piece {
            attacks_with(piece, origin_square, self.get_board().get_occupied())
        } else {
            Bitboard::EMPTY
        }
    }

    fn king_is_in_check(&self) -> bool {
        let attacker = self.get_side_to_move().get_opponent();
        let king_square = self.get_board().get_king_position(attacker.get_opponent());

        self.attacks(attacker).is_set(king_square)
    }

    fn king_can_be_captured(&self) -> bool {
        let attacker = self.get_side_to_move();
        let king_square = self.get_board().get_king_position(attacker.get_opponent());

        self.attacks(attacker).is_set(king_square)
    }
}

#[cfg(test)]
mod tests {
    extern crate indoc;

    use super::*;
    use indoc::indoc;

    #[test]
    fn black_attacks_from_starting_pos() {
        assert_eq!(
            GameState::get_starting_position()
                .attacks(Color::BLACK)
                .to_string(),
            indoc!(
                "

                   A B C D E F G H
                 ┌─────────────────┐
                8│ 0 1 1 1 1 1 1 0 │8
                7│ 1 1 1 1 1 1 1 1 │7
                6│ 1 1 1 1 1 1 1 1 │6
                5│ 0 0 0 0 0 0 0 0 │5
                4│ 0 0 0 0 0 0 0 0 │4
                3│ 0 0 0 0 0 0 0 0 │3
                2│ 0 0 0 0 0 0 0 0 │2
                1│ 0 0 0 0 0 0 0 0 │1
                 └─────────────────┘
                   A B C D E F G H
            "
            )
        );
    }

    #[test]
    fn black_knight_attacks_from_starting_pos() {
        let game_state = GameState::get_starting_position();
        let piece = Piece::new(Color::BLACK, PieceKind::KNIGHT);
        assert_eq!(
            game_state.attacks_with_piece(piece).to_string(),
            indoc!(
                "

                   A B C D E F G H
                 ┌─────────────────┐
                8│ 0 0 0 0 0 0 0 0 │8
                7│ 0 0 0 1 1 0 0 0 │7
                6│ 1 0 1 0 0 1 0 1 │6
                5│ 0 0 0 0 0 0 0 0 │5
                4│ 0 0 0 0 0 0 0 0 │4
                3│ 0 0 0 0 0 0 0 0 │3
                2│ 0 0 0 0 0 0 0 0 │2
                1│ 0 0 0 0 0 0 0 0 │1
                 └─────────────────┘
                   A B C D E F G H
            "
            )
        );
    }
}
