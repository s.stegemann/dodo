// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

use std::sync::mpsc::{channel, Receiver, Sender, TryRecvError};
use std::sync::{Arc, Mutex};
use std::thread::spawn;
use std::time::Instant;

use derive_more::Constructor;

use crate::evaluation::{evaluate_interior_node, evaluate_statically, Score, ScoreRange};
use crate::representation::{Color, GameState, Move, MoveSet};
use crate::search::{
    Attacks, ClockState, MoveGenerator, SearchLimit, SearchOptions, TranspositionTable,
};
use crate::util::plies_to_moves;

const MIN_DEPTH: i32 = 1;
const ESTIMATED_MOVES_TO_GO: i32 = 50;

pub trait UpdateHandler = Fn(SearchUpdateEvent) + Send;
pub trait FinishedHandler = Fn(SearchFinishedEvent) + Send;

#[derive(Debug, Constructor, Copy, Clone)]
pub struct SearchUpdateEvent {
    pub current_move: Move,
    pub current_score: ScoreRange,
    pub depth: i32,
}

#[derive(Debug, Constructor, Copy, Clone)]
pub struct SearchFinishedEvent {
    pub best_move: Move,
}

enum SearchCommand {
    StartSearching {
        history: Vec<GameState>,
        search_moves: MoveSet,
        limits: Vec<SearchLimit>,
    },
    StopSearching,
}

#[derive(Debug)]
pub struct SearchManager {
    command_channel: Sender<SearchCommand>,
}

#[derive(Debug)]
struct SearchWorker<U: UpdateHandler + 'static, F: FinishedHandler + 'static> {
    command_channel: Receiver<SearchCommand>,
    transposition_table: Arc<Mutex<TranspositionTable>>,
    history: Vec<GameState>,
    depth_from_root: i32,
    started_search: Instant,
    our_side: Color,
    limits: Vec<SearchLimit>,
    on_update: U,
    on_finished: F,
}

impl SearchManager {
    pub fn new(
        transposition_table: Arc<Mutex<TranspositionTable>>,
        on_update: impl UpdateHandler + 'static,
        on_finished: impl FinishedHandler + 'static,
    ) -> Self {
        let (tx, rx) = channel();

        spawn(move || SearchWorker::new(rx, transposition_table, on_update, on_finished).run());

        Self {
            command_channel: tx,
        }
    }

    pub fn start_searching(&mut self, history: &[GameState], search_options: SearchOptions) {
        let current_game_state = history.last().expect("history can not be empty.");

        let search_moves = match search_options.search_moves {
            Some(search_moves) => search_moves & current_game_state.legal_moves(),
            None => current_game_state.legal_moves(),
        };

        if search_moves.is_empty() {
            return;
        }

        self.command_channel
            .send(SearchCommand::StartSearching {
                history: history.to_vec(),
                search_moves,
                limits: search_options.search_limits,
            })
            .expect("The searcher worker should be running.");
    }

    pub fn stop_searching(&mut self) {
        self.command_channel
            .send(SearchCommand::StopSearching)
            .expect("The searcher worker should be running.");
    }
}

impl<U: UpdateHandler + 'static, F: FinishedHandler + 'static> SearchWorker<U, F> {
    fn new(
        command_channel: Receiver<SearchCommand>,
        transposition_table: Arc<Mutex<TranspositionTable>>,
        on_update: U,
        on_finished: F,
    ) -> Self {
        Self {
            history: vec![],
            depth_from_root: 0,
            command_channel,
            transposition_table,
            started_search: Instant::now(),
            our_side: Color::WHITE,
            limits: vec![],
            on_update,
            on_finished,
        }
    }

    fn run(&mut self) {
        while let Ok(command) = self.command_channel.recv() {
            if let SearchCommand::StartSearching {
                history,
                search_moves,
                limits,
            } = command
            {
                self.our_side = history
                    .last()
                    .expect("history must not be empty")
                    .get_side_to_move();
                self.depth_from_root = 0;
                self.history = history;
                self.limits = limits;
                self.started_search = Instant::now();
                self.iterative_deepening(search_moves);
            }
        }
    }

    fn iterative_deepening(&mut self, search_moves: MoveSet) {
        let mut best_move = *search_moves
            .first()
            .expect("search_moves can not be empty.");
        let mut best_score;

        for depth in MIN_DEPTH.. {
            (best_move, best_score) = self.choose_best_move(&search_moves, depth);

            (self.on_update)(SearchUpdateEvent::new(
                best_move,
                ScoreRange::Exact(best_score),
                depth,
            ));

            match self.command_channel.try_recv() {
                Ok(SearchCommand::StopSearching) => break,
                Err(TryRecvError::Empty) => (),
                Err(error) => {
                    panic!("Error occured while search thread tried to receive command: {error}")
                }
                _ => todo!(),
            }

            if self.should_stop_search(depth) {
                break;
            }
        }

        (self.on_finished)(SearchFinishedEvent::new(best_move));
    }

    fn make_move(&mut self, move_: Move) {
        self.depth_from_root += 1;
        let mut new_game_state = self.get_current_game_state().clone();
        debug_assert!(new_game_state.is_pseudolegal_move(move_));
        new_game_state.make_move_unchecked(move_);
        self.history.push(new_game_state);
    }

    fn unmake_move(&mut self) {
        self.depth_from_root -= 1;
        self.history.pop();
    }

    fn get_current_game_state(&self) -> &GameState {
        self.history
            .last()
            .expect("self.history can never be empty.")
    }

    // Is called during iterative deepening after a new depth is reached to determine, whether
    // reaching next depth should be attempted.
    fn should_stop_search(&self, depth: i32) -> bool {
        use SearchLimit::*;

        self.limits.iter().any(|limit| match limit {
            Clock(clock_state) => self.should_stop_search_time_management(clock_state),
            Depth(max_depth) => depth >= *max_depth,
            Nodes(_max_nodes) => todo!(),
            Mate(_max_depth) => todo!(),
            Time(stop_instant) => Instant::now() >= *stop_instant,
            Infinite => false,
        })
    }

    fn should_stop_search_time_management(&self, clock_state: &ClockState) -> bool {
        let moves_to_go = clock_state
            .moves_to_go
            .unwrap_or(ESTIMATED_MOVES_TO_GO)
            .try_into()
            .expect("movestogo must not be negative");

        let our_clock = if let Some(clock) = clock_state.clocks[self.our_side] {
            clock
        } else {
            return false;
        };

        let targeted_duration =
            (our_clock.clock_ends - self.started_search) / moves_to_go + our_clock.increment;

        self.started_search.elapsed() >= targeted_duration
    }

    /// Searches to `depth` and returns the best move out of `search_moves`.
    /// `search_moves` must not be empty and only contain legal moves.
    fn choose_best_move(&mut self, search_moves: &MoveSet, depth: i32) -> (Move, Score) {
        let mut best_move = *search_moves
            .first()
            .expect("search_moves must not be empty.");
        let mut best_score = Score::Min;

        for move_ in search_moves {
            self.make_move(*move_);
            let score = -self.alpha_beta(Score::Min, -best_score, depth - 1);
            self.unmake_move();

            if score > best_score {
                best_move = *move_;
                best_score = score;
            }
        }

        (best_move, best_score)
    }

    /// Searches the current position to a given depth and returns an evaluation.
    ///
    /// If the returned score is inside the search window (alpha, beta], it is an exact score.
    /// If it is greater than beta, a beta-cutoff happened and the score is only a lower bound.
    /// If it is equal to alpha, alpha is an upper bound for the evaluation.
    fn alpha_beta(&mut self, mut alpha: Score, beta: Score, depth: i32) -> Score {
        let current_game_state = self.get_current_game_state();

        // End the search with a heuristic evaluation, if the target depth is reached.
        if depth == 0 {
            return evaluate_statically(current_game_state);
        }

        // Return early if we can already assign a value to the current node without a search.
        if let Some(score) = evaluate_interior_node(&self.history) {
            return score;
        }

        // Return early if we already searched this position with sufficient depth.
        if let Some(tt_entry) = self
            .transposition_table
            .lock()
            .unwrap()
            .lookup(current_game_state)
        {
            if tt_entry.depth >= depth {
                use ScoreRange::*;

                // TODO: this does not look right, check this logic later.
                return match tt_entry.score_range {
                    UpperBound(score) => score,
                    LowerBound(score) => score,
                    Exact(score) => score,
                };
            }
        }

        let mut found_legal_move = false;

        // If we find a move, that improves alpha we want to save it into the transposition table.
        let mut best_move: Option<Move> = None;

        // Recurse the search for each pseudolegal move.
        for candidate_move in current_game_state.pseudolegal_moves() {
            self.make_move(candidate_move);
            let score = -self.alpha_beta(-beta, -alpha, depth - 1);

            // If candidate_move did not leave its own king in check it was legal.
            if score > Score::Min {
                found_legal_move = true
            }

            // Beta-Cutoff: this node does not influence the result, because `candidate_move` is so
            // good for the side to move, that the other side will not allow this line.
            if score >= beta {
                self.unmake_move();
                // Store a lower bound in the transposition table.
                // The score found is a lower bound, because there might be better moves, that we have not
                // searched yet.

                self.transposition_table.lock().unwrap().store(
                    self.get_current_game_state(),
                    ScoreRange::LowerBound(score),
                    depth,
                    Some(candidate_move),
                );

                return beta;
            }

            if score >= alpha {
                best_move = Some(candidate_move);
                alpha = score;
            }
            self.unmake_move();
        }

        // Recognize mate and stalemate.
        if !found_legal_move {
            if self.get_current_game_state().king_is_in_check() {
                alpha = Score::GettingMatedIn(plies_to_moves(self.depth_from_root));
            } else {
                alpha = Score::Centipawns(0);
            }
        }

        // Store evaluation and possibly a best move for the current position.
        self.transposition_table.lock().unwrap().store(
            self.get_current_game_state(),
            // If we found a move that improves alpha, but does not lead to an beta-cutoff the corresponding score is exact.
            // If no move improved alpha the score is a lower bound.
            if best_move.is_some() {
                ScoreRange::Exact(alpha)
            } else {
                ScoreRange::UpperBound(alpha)
            },
            depth,
            best_move,
        );
        alpha
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsing::FenConvertable;

    const DEFAULT_HASH_SIZE: i64 = 16;

    fn expect_move_in_position(fen: &str, expected_move: &str, expected_score: Score, depth: i32) {
        let game_state = GameState::from_fen(fen).unwrap();
        let expected_move = expected_move.try_into().unwrap();
        dbg!(&game_state);

        let transposition_table = Arc::new(Mutex::new(TranspositionTable::new(DEFAULT_HASH_SIZE)));
        let mut worker = SearchWorker::new(channel().1, transposition_table, |_| (), |_| ());
        worker.history = vec![game_state.clone()];

        assert_eq!(
            worker.choose_best_move(&game_state.legal_moves(), depth),
            (expected_move, expected_score),
        );
    }

    #[test]
    fn find_mate_in_one() {
        expect_move_in_position(
            "5r1k/4R1p1/7p/P7/4P3/2Q4P/P5P1/R6K w - - 1 30",
            "c3g7",
            Score::MatingIn(1),
            3,
        )
    }

    #[test]
    fn find_mate_in_two_with_zugzwang() {
        expect_move_in_position(
            "8/p7/k1K5/8/1p3P2/5R2/8/4B3 w - - 2 1",
            "f3b3",
            Score::MatingIn(2),
            5,
        )
    }

    #[test]
    fn sacrifice_material_to_prevent_draw_by_repetition() {
        let game_state = GameState::from_fen("6kq/6pp/8/8/6P1/5P1b/4PK2/8 w - - 0 1").unwrap();

        let transposition_table = Arc::new(Mutex::new(TranspositionTable::new(DEFAULT_HASH_SIZE)));
        let mut worker = SearchWorker::new(channel().1, transposition_table, |_| (), |_| ());
        worker.history = vec![game_state];
        worker.our_side = Color::BLACK;

        // Setup moves to prepare repetition.
        for move_ in ["f2g3", "h3f1", "g3f2", "f1h3", "f2g3", "h3f1", "g3f2"] {
            let move_ = move_.try_into().unwrap();
            assert!(worker.get_current_game_state().is_legal_move(move_));
            worker.make_move(move_);
        }

        dbg!(worker.get_current_game_state());

        // Every move other than f1h3 loses the bishop, but f1h3 is a draw by repetition.
        // Black should sacrifice the bishop, because he is far ahead.
        assert_ne!(
            worker
                .choose_best_move(&worker.get_current_game_state().legal_moves(), 4)
                .0,
            "f1h3".try_into().unwrap(),
        );
    }
}
