// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

use crate::evaluation::{Score, ScoreRange};
use crate::representation::{GameState, Move, ZobristHash};

#[derive(Debug, Clone)]
pub struct TranspositionTable {
    entries: Vec<TranspositionTableEntry>,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct TranspositionTableEntry {
    key: ZobristHash,
    pub score_range: ScoreRange,
    pub depth: i32,
    pub best_or_refutation_move: Option<Move>,
}

impl Default for TranspositionTableEntry {
    fn default() -> Self {
        TranspositionTableEntry {
            key: ZobristHash::default(),
            score_range: ScoreRange::Exact(Score::Centipawns(0)),
            depth: 0,
            best_or_refutation_move: None,
        }
    }
}

impl TranspositionTable {
    pub fn num_entries_for_size(size_in_mb: i64) -> usize {
        size_in_mb as usize * 1024 * 1024 / std::mem::size_of::<TranspositionTableEntry>()
    }

    pub fn new(size_in_mb: i64) -> TranspositionTable {
        TranspositionTable {
            entries: vec![
                TranspositionTableEntry::default();
                Self::num_entries_for_size(size_in_mb)
            ],
        }
    }

    pub fn lookup(&self, game_state: &GameState) -> Option<TranspositionTableEntry> {
        let key = game_state.get_hash();
        let index = key.as_usize() % self.entries.len();
        let entry = &self.entries[index];

        if entry.key == key {
            Some(*entry)
        } else {
            None
        }
    }

    pub fn store(
        &mut self,
        game_state: &GameState,
        score_range: ScoreRange,
        depth: i32,
        best_or_refutation_move: Option<Move>,
    ) {
        let key = game_state.get_hash();
        let index = key.as_usize() % self.entries.len();
        self.entries[index] = TranspositionTableEntry {
            key,
            score_range,
            depth,
            best_or_refutation_move,
        };
    }

    pub fn clear(&mut self) {
        for entry in self.entries.iter_mut() {
            entry.key = ZobristHash::default();
            entry.score_range = ScoreRange::Exact(Score::Centipawns(0));
            entry.depth = 0;
        }
    }

    pub fn resize(&mut self, size_in_mb: i64) {
        self.clear();

        self.entries.resize(
            Self::num_entries_for_size(size_in_mb),
            TranspositionTableEntry::default(),
        );
    }
}
