// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2022 Sven Stegemann

use rand_chacha::ChaChaRng;
use rand_core::{RngCore, SeedableRng};
use u64;

use strum::{EnumCount, IntoEnumIterator};

use crate::representation::{CastlingType, Color, File, GameState, Piece, PieceKind, Square};

#[derive(Debug, Default, Copy, Clone, Eq, PartialEq, Hash)]
pub struct ZobristHash(u64);

const NUM_KEYS: usize = Color::COUNT * PieceKind::COUNT * Square::COUNT // for each piece on every square
    + File::COUNT // for each en passant square
    + Color::COUNT * CastlingType::COUNT // for each castling right
    + 1; // side to move
const SEED: u64 = u64::from_be_bytes(*b"Zobrist!");

lazy_static! {
    static ref KEYS: [u64; NUM_KEYS] = {
        let mut keys = [0; NUM_KEYS];
        let mut prng = ChaChaRng::seed_from_u64(SEED);

        for key in keys.iter_mut() {
            *key = prng.next_u64();
        }

        keys
    };
    static ref SIDE_TO_MOVE_KEY: u64 = KEYS[0];
    static ref CASTLE_RIGHT_KEYS: &'static [u64] = &KEYS[1..5];
    static ref EN_PASSANT_KEYS: &'static [u64] = &KEYS[5..13];
    static ref BOARD_KEYS: &'static [u64] = &KEYS[13..];
}

impl ZobristHash {
    pub fn from_game_state(game_state: &GameState) -> ZobristHash {
        let mut result = ZobristHash(0);

        if game_state.get_side_to_move() == Color::BLACK {
            result.flip_side_to_move();
        }

        for color in Color::iter() {
            for castling_type in CastlingType::iter() {
                if game_state.can_castle_to_side(color, castling_type) {
                    result.flip_castling_right(color, castling_type);
                }
            }
        }

        if let Some(square) = game_state.get_en_passant_square() {
            result.flip_en_passant_square(square);
        }

        let board = game_state.get_board();
        for square in board.get_occupied() {
            let piece = board.get_piece_on_square(square).unwrap();
            result.flip_piece_on_square(piece, square);
        }

        result
    }

    pub fn flip_side_to_move(&mut self) {
        self.0 ^= *SIDE_TO_MOVE_KEY;
    }

    pub fn flip_castling_right(&mut self, color: Color, castling_type: CastlingType) {
        let index = color as usize * CastlingType::COUNT + castling_type as usize;
        self.0 ^= CASTLE_RIGHT_KEYS[index];
    }

    pub fn flip_en_passant_square(&mut self, square: Square) {
        self.0 ^= EN_PASSANT_KEYS[square.get_file() as usize];
    }

    pub fn flip_piece_on_square(&mut self, piece: Piece, square: Square) {
        let mut index = piece.color as usize;
        index = index * PieceKind::COUNT + piece.kind as usize;
        index = index * Square::COUNT + square as usize;
        self.0 ^= BOARD_KEYS[index];
    }

    pub fn as_u64(&self) -> u64 {
        self.0
    }

    pub fn as_usize(&self) -> usize {
        self.0 as usize
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_zobrist_hash_of_starting_position() {
        let game_state = GameState::get_starting_position();
        let hash = ZobristHash::from_game_state(&game_state);

        assert_eq!(hash, ZobristHash(0x4e4bd493daa4c8e));
    }

    #[test]
    fn test_all_keys_are_unique() {
        use itertools::Itertools;

        assert_eq!(KEYS.len(), KEYS.into_iter().unique().count());
    }

    #[test]
    fn test_different_key_slices_are_partition_of_keys() {
        use std::collections::HashSet;

        let keys = HashSet::from_iter(KEYS.iter().cloned());
        let mut partition: HashSet<u64> = HashSet::new();

        partition.insert(*SIDE_TO_MOVE_KEY);
        partition.extend(CASTLE_RIGHT_KEYS.iter());
        partition.extend(EN_PASSANT_KEYS.iter());
        partition.extend(BOARD_KEYS.iter());

        assert_eq!(keys, partition);
        assert_eq!(
            1 + CASTLE_RIGHT_KEYS.len() + EN_PASSANT_KEYS.len() + BOARD_KEYS.len(),
            keys.len()
        );
    }
}
