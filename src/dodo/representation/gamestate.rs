// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

extern crate enum_map;
extern crate strum;
extern crate strum_macros;

use enum_map::{enum_map, Enum, EnumMap};
use strum::IntoEnumIterator;
use strum_macros::{EnumCount, EnumIter};

use crate::representation::{
    BoardState, Color, File, Move, Piece, PieceKind, Promotion, Rank, Square, Vector, ZobristHash,
    STARTING_POSITION_BOARD,
};

#[derive(Debug, Copy, Clone, Eq, PartialEq, EnumCount, EnumIter, Enum)]
pub enum CastlingType {
    KINGSIDE,
    QUEENSIDE,
}

pub type CastlingRights = EnumMap<Color, EnumMap<CastlingType, bool>>;

#[derive(Debug, Clone, Eq)]
pub struct GameState {
    board: BoardState,
    side_to_move: Color,
    castling_rights: CastlingRights,
    en_passant_square: Option<Square>,
    fifty_move_counter: i8,
    hash: ZobristHash,
}

impl GameState {
    pub fn get_starting_position() -> GameState {
        let mut result = GameState {
            board: *STARTING_POSITION_BOARD,
            side_to_move: Color::WHITE,
            castling_rights: enum_map! {
                Color::WHITE => enum_map! {
                    CastlingType::KINGSIDE => true,
                    CastlingType::QUEENSIDE => true
                },
                Color::BLACK => enum_map! {
                    CastlingType::KINGSIDE => true,
                    CastlingType::QUEENSIDE => true
                },
            },
            en_passant_square: None,
            fifty_move_counter: 0,
            hash: ZobristHash::default(),
        };

        result.hash = ZobristHash::from_game_state(&result);

        result
    }

    pub(crate) fn new(
        board: BoardState,
        side_to_move: Color,
        castling_rights: CastlingRights,
        en_passant_square: Option<Square>,
        fifty_move_counter: i8,
    ) -> GameState {
        let mut result = GameState {
            board,
            side_to_move,
            castling_rights,
            en_passant_square,
            fifty_move_counter,
            hash: ZobristHash::default(),
        };

        result.hash = ZobristHash::from_game_state(&result);

        result
    }

    pub fn get_board(&self) -> &BoardState {
        &self.board
    }

    pub fn get_side_to_move(&self) -> Color {
        self.side_to_move
    }

    pub fn can_castle_to_side(&self, color: Color, castling_type: CastlingType) -> bool {
        self.castling_rights[color][castling_type]
    }

    pub fn can_castle(&self, color: Color) -> bool {
        self.can_castle_to_side(color, CastlingType::KINGSIDE)
            || self.can_castle_to_side(color, CastlingType::QUEENSIDE)
    }

    pub fn get_en_passant_square(&self) -> Option<Square> {
        self.en_passant_square
    }

    pub fn get_fifty_move_counter(&self) -> i8 {
        self.fifty_move_counter
    }

    fn set_piece(&mut self, piece: Piece, square: Square) {
        self.board.set_piece(piece, square);
        self.hash.flip_piece_on_square(piece, square);
    }

    fn unset_piece(&mut self, piece: Piece, square: Square) {
        self.board.unset_piece(piece, square);
        self.hash.flip_piece_on_square(piece, square);
    }

    fn remove_castling_right(&mut self, color: Color, castling_type: CastlingType) {
        if self.castling_rights[color][castling_type] {
            self.castling_rights[color][castling_type] = false;
            self.hash.flip_castling_right(color, castling_type);
        }
    }

    pub fn make_move(&mut self, move_: Move) {
        use crate::search::MoveGenerator;

        if self.is_legal_move(move_) {
            self.make_move_unchecked(move_);
        } else {
            panic!("Tried to play illegal move {move_}.");
        }
    }

    pub fn make_move_unchecked(&mut self, move_: Move) {
        let moving_piece = self
            .board
            .get_piece_on_square(move_.from)
            .expect("Invalid move: Can not move from empty square.");
        let mut is_capturing_move = false;

        // If this is a castling move, move the rook, too.
        if moving_piece.kind == PieceKind::KING && move_.from.get_file() == File::E {
            if let Some((rook_old_file, rook_new_file)) = match move_.to.get_file() {
                File::C => Some((File::A, File::D)), // Queenside castling
                File::G => Some((File::H, File::F)), // Kingside castling
                _ => None,                           // Non-castling king move.
            } {
                let castling_rank = Rank::get_relative_to_side(self.side_to_move, Rank::_1);
                self.unset_piece(
                    Piece::new(self.side_to_move, PieceKind::ROOK),
                    Square::new(rook_old_file, castling_rank),
                );
                self.set_piece(
                    Piece::new(self.side_to_move, PieceKind::ROOK),
                    Square::new(rook_new_file, castling_rank),
                );
            }
        }

        // Remove moving piece from origin square.
        self.unset_piece(moving_piece, move_.from);

        // Remove captured piece.
        if moving_piece.kind == PieceKind::PAWN && self.en_passant_square == Some(move_.to) {
            // Remove en passant captured pawn.
            self.unset_piece(
                Piece::new(self.side_to_move.get_opponent(), PieceKind::PAWN),
                Square::new(
                    move_.to.get_file(),
                    Rank::get_relative_to_side(self.side_to_move, Rank::_5),
                ),
            );

            is_capturing_move = true;
        } else if let Some(captured_piece) = self.board.get_piece_on_square(move_.to) {
            self.unset_piece(captured_piece, move_.to);

            is_capturing_move = true;
        }

        // Add moving piece to target square.
        let possibly_promoted_piece = match move_.promotion {
            Promotion::NONE => moving_piece,
            Promotion::KNIGHT => Piece::new(moving_piece.color, PieceKind::KNIGHT),
            Promotion::BISHOP => Piece::new(moving_piece.color, PieceKind::BISHOP),
            Promotion::ROOK => Piece::new(moving_piece.color, PieceKind::ROOK),
            Promotion::QUEEN => Piece::new(moving_piece.color, PieceKind::QUEEN),
        };
        self.set_piece(possibly_promoted_piece, move_.to);

        // Update castling rights.
        match moving_piece.kind {
            PieceKind::KING => {
                for castling_type in CastlingType::iter() {
                    self.remove_castling_right(self.side_to_move, castling_type);
                }
            }
            PieceKind::ROOK
                if move_.from
                    == Square::new(
                        File::A,
                        Rank::get_relative_to_side(self.side_to_move, Rank::_1),
                    ) =>
            {
                self.remove_castling_right(self.side_to_move, CastlingType::QUEENSIDE);
            }
            PieceKind::ROOK
                if move_.from
                    == Square::new(
                        File::H,
                        Rank::get_relative_to_side(self.side_to_move, Rank::_1),
                    ) =>
            {
                self.remove_castling_right(self.side_to_move, CastlingType::KINGSIDE);
            }
            _ => (),
        }
        let backrank = Rank::get_relative_to_side(self.side_to_move, Rank::_8);
        if move_.to.get_rank() == backrank {
            match move_.to.get_file() {
                File::A => self.remove_castling_right(
                    self.side_to_move.get_opponent(),
                    CastlingType::QUEENSIDE,
                ),
                File::H => self.remove_castling_right(
                    self.side_to_move.get_opponent(),
                    CastlingType::KINGSIDE,
                ),
                _ => (),
            }
        }

        // Remove old en passant square.
        if let Some(old_ep_square) = self.en_passant_square {
            self.hash.flip_en_passant_square(old_ep_square);
            self.en_passant_square = None;
        }

        // Possibly set new en passant square;
        if moving_piece.kind == PieceKind::PAWN
            && move_.from.get_rank() == Rank::get_relative_to_side(self.side_to_move, Rank::_2)
            && move_.to.get_rank() == Rank::get_relative_to_side(self.side_to_move, Rank::_4)
        {
            // Move is a pawn double push.

            let enemy_pawns = self.board.get_positions(Piece::new(
                self.side_to_move.get_opponent(),
                PieceKind::PAWN,
            ));

            let enemy_can_en_passant = [Vector::EAST, Vector::WEST]
                .iter()
                .map(|vector| move_.to + *vector)
                .filter_map(|square| square.ok())
                .any(|square| enemy_pawns.is_set(square));

            if enemy_can_en_passant {
                let new_ep_square = Square::new(
                    move_.from.get_file(),
                    Rank::get_relative_to_side(self.side_to_move, Rank::_3),
                );
                self.hash.flip_en_passant_square(new_ep_square);
                self.en_passant_square = Some(new_ep_square);
            }
        }

        // Update side to move.
        self.side_to_move = self.side_to_move.get_opponent();
        self.hash.flip_side_to_move();

        // Update fifty_move_counter.
        self.fifty_move_counter = if is_capturing_move || moving_piece.kind == PieceKind::PAWN {
            0
        } else {
            self.fifty_move_counter + 1
        }
    }

    pub fn get_hash(&self) -> ZobristHash {
        self.hash
    }
}

impl PartialEq for GameState {
    fn eq(&self, other: &Self) -> bool {
        self.board == other.board
            && self.side_to_move == other.side_to_move
            && self.castling_rights == other.castling_rights
            && self.en_passant_square == other.en_passant_square
    }
}

impl Default for GameState {
    fn default() -> Self {
        Self::get_starting_position()
    }
}
