// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

use super::square::{File, Rank, Square};

/// A simple bitboard implementation
///
/// ## Example:
///
/// ```rust
/// use dodo::representation::{Bitboard, Square};
///
/// let mut bitboard = Bitboard::EMPTY;
/// bitboard.set(Square::B3);
/// assert!(bitboard.is_set(Square::B3));
///
/// assert!(Bitboard::EMPTY | Bitboard::FULL == Bitboard::FULL);
/// ```
#[derive(Copy, Clone, Eq, PartialEq, Default)]
pub struct Bitboard(u64);

impl Bitboard {
    pub const EMPTY: Self = Self(0);
    pub const FULL: Self = Self(std::u64::MAX);

    /// Checks whether all squares are unset.
    pub const fn is_empty(self) -> bool {
        self.0 == 0
    }

    /// Creates a new Bitboard where only `square` is set.
    pub const fn from_square(square: Square) -> Self {
        Bitboard(1 << square.to_index())
    }

    /// Creates a new Bitboard where only `squares` are set.
    pub fn from_squares(squares: &[Square]) -> Self {
        let mut result = Bitboard::EMPTY;

        for square in squares {
            result.set(*square)
        }

        result
    }

    pub fn to_squares(self) -> Vec<Square> {
        self.into_iter().collect()
    }

    pub const fn from_u64(bits: u64) -> Self {
        Self(bits)
    }

    pub const fn to_u64(self) -> u64 {
        self.0
    }

    pub fn set(&mut self, square: Square) {
        *self |= Bitboard::from_square(square);
    }

    pub fn unset(&mut self, square: Square) {
        *self &= !(Bitboard::from_square(square));
    }

    pub fn is_set(self, square: Square) -> bool {
        Bitboard::EMPTY != self & Bitboard::from_square(square)
    }

    pub fn popcount(self) -> u32 {
        self.0.count_ones()
    }

    // TODO: Implement this as a macro?
    // /// Parses a bitboard from an compile time string.
    // ///
    // /// This allows us to define certain constant bitboards in the source code.
    // ///
    // /// Example:
    // ///
    // /// ```rust, ignore
    // /// use dodo::representation::{Bitboard, Square};
    // ///
    // /// let diagonal = Bitboard::from_string("
    // ///     00000001
    // ///     00000010
    // ///     00000100
    // ///     00001000
    // ///     00010000
    // ///     00100000
    // ///     01000000
    // ///     10000000
    // /// ");
    // ///
    // /// assert!(diagonal.is_set(Square::A1));
    // /// ```
    // pub const fn from_string(string: &str) -> Bitboard {
    //     let mut file_index: i8 = 0;
    //     let mut rank_index: i8 = 0;
    //     let mut string_index: usize = 0;
    //     let bytes = string.as_bytes();

    //     let mut result = Bitboard::EMPTY;

    //     // TODO: Replace this with an for loop if a newer version of Rust
    //     //       allows this.
    //     // for byte in bytes {
    //     //     match byte {
    //     //         b'1' | b'0' => {
    //     //             // TODO: Zu Ende programmieren.
    //     //             // let square = Square.new()
    //     //             // result.set()
    //     //         },
    //     //         b' ' | b'\n' => (),
    //     //         _ => panic!("Unexpected character in bitboard string.")
    //     //     }
    //     // }

    //     result
    // }
}

impl<T: Into<Bitboard>> std::ops::BitAnd<T> for Bitboard {
    type Output = Self;

    fn bitand(self, other: T) -> Self {
        Self(self.0 & other.into().0)
    }
}

impl<T: Into<Bitboard>> std::ops::BitAndAssign<T> for Bitboard {
    fn bitand_assign(&mut self, other: T) {
        self.0 &= other.into().0
    }
}

impl<T: Into<Bitboard>> std::ops::BitOr<T> for Bitboard {
    type Output = Self;

    fn bitor(self, other: T) -> Self {
        Self(self.0 | other.into().0)
    }
}

impl<T: Into<Bitboard>> std::ops::BitOrAssign<T> for Bitboard {
    fn bitor_assign(&mut self, other: T) {
        self.0 |= other.into().0
    }
}

impl<T: Into<Bitboard>> std::ops::BitXor<T> for Bitboard {
    type Output = Self;

    fn bitxor(self, other: T) -> Self {
        Self(self.0 ^ other.into().0)
    }
}

impl<T: Into<Bitboard>> std::ops::BitXorAssign<T> for Bitboard {
    fn bitxor_assign(&mut self, other: T) {
        self.0 ^= other.into().0
    }
}

impl const std::ops::Not for Bitboard {
    type Output = Self;

    fn not(self) -> Self {
        Self(!self.0)
    }
}

impl const From<Square> for Bitboard {
    fn from(square: Square) -> Self {
        Self(1 << square.to_index())
    }
}

impl const From<Rank> for Bitboard {
    fn from(rank: Rank) -> Self {
        Self(0x00_00_00_00_00_00_00_ff << (rank.to_index() * 8))
    }
}

impl const From<File> for Bitboard {
    fn from(file: File) -> Self {
        Self(0x01_01_01_01_01_01_01_01 << file.to_index())
    }
}

impl const From<u64> for Bitboard {
    fn from(bitboard: u64) -> Self {
        Self(bitboard)
    }
}

impl const From<Bitboard> for u64 {
    fn from(bitboard: Bitboard) -> u64 {
        bitboard.0
    }
}

pub struct BitboardIterator {
    remaining_bits: Bitboard,
}

impl Iterator for BitboardIterator {
    type Item = Square;

    fn next(&mut self) -> Option<Self::Item> {
        if self.remaining_bits == Bitboard::EMPTY {
            None
        } else {
            let leading_zeroes: i8 = self.remaining_bits.0.leading_zeros().try_into().unwrap();
            let next_square = Square::from_index(63i8 - leading_zeroes).unwrap();
            self.remaining_bits ^= next_square;
            Some(next_square)
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(64))
    }
}

impl IntoIterator for Bitboard {
    type Item = Square;
    type IntoIter = BitboardIterator;

    fn into_iter(self) -> Self::IntoIter {
        BitboardIterator {
            remaining_bits: self,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn convert_squares_and_bitboards() {
        let Bitboard(bitboard_representation) = Bitboard::from_square(Square::G8);

        assert_eq!(bitboard_representation, 1 << (7 * 8 + 6));
    }

    #[test]
    fn union_of_bitboard_and_negation_are_full_bitboard() {
        let bitboard = Bitboard(0x01_23_45_67_89_ab_cd_ef);

        assert_eq!(Bitboard::FULL, bitboard | !bitboard)
    }

    #[test]
    fn intersection_of_bitboard_and_negation_are_empty_bitboard() {
        let bitboard: Bitboard = Bitboard(0x01_23_45_67_89_ab_cd_ef);

        assert_eq!(Bitboard::EMPTY, bitboard & !bitboard)
    }

    #[test]
    fn set_square() {
        let mut bitboard = Bitboard::EMPTY;
        let square = Square::C4;

        bitboard.set(square);
        assert!(bitboard.is_set(square))
    }

    #[test]
    fn unset_square() {
        let mut bitboard = Bitboard::FULL;
        let square = Square::C4;

        bitboard.unset(square);
        assert!(!bitboard.is_set(square))
    }
}
