// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

pub extern crate strum;
pub extern crate strum_macros;

use std::mem;
use std::ops::{Add, Mul, Sub};
use strum_macros::{EnumCount, EnumIter};

use crate::representation::Color;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum CoordinateArithmeticsError {
    IntegerOverflowOccured,
    ResultOutOfRange,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, EnumCount, EnumIter, Hash)]
#[repr(i8)]
pub enum Rank {
    _1,
    _2,
    _3,
    _4,
    _5,
    _6,
    _7,
    _8,
}

impl Rank {
    pub const fn to_index(&self) -> i8 {
        *self as i8
    }

    pub fn from_index(index: i8) -> Option<Rank> {
        match index {
            0..=7 => unsafe { Some(Rank::unsafe_from_index(index)) },
            _ => None,
        }
    }

    unsafe fn unsafe_from_index(index: i8) -> Rank {
        debug_assert!(index >= 0);
        debug_assert!(index <= 7);
        ::std::mem::transmute(index)
    }

    pub fn get_relative_to_side(color: Color, rank: Rank) -> Rank {
        match color {
            Color::WHITE => rank,
            Color::BLACK => unsafe { Rank::unsafe_from_index(7 - rank.to_index()) },
        }
    }
}

impl<T: Into<i8>> Add<T> for Rank {
    type Output = Result<Rank, CoordinateArithmeticsError>;

    fn add(self, other: T) -> Result<Rank, CoordinateArithmeticsError> {
        let self_index = self as i8; // Convert rank to index (0...7)
        let offset = other.into();

        match self_index.checked_add(offset) {
            Some(result_index @ 0..=7) => unsafe { Ok(Rank::unsafe_from_index(result_index)) },
            None => Err(CoordinateArithmeticsError::IntegerOverflowOccured),
            _ => Err(CoordinateArithmeticsError::ResultOutOfRange),
        }
    }
}

impl<OffsetType: Into<i8>> Sub<OffsetType> for Rank {
    type Output = Result<Rank, CoordinateArithmeticsError>;

    fn sub(self, other: OffsetType) -> Result<Rank, CoordinateArithmeticsError> {
        self + (-other.into())
    }
}

impl Sub<Rank> for Rank {
    type Output = i8;

    fn sub(self, other: Rank) -> i8 {
        self as i8 - other as i8
    }
}

// impl<OffsetType: Into<i8>> AddAssign<OffsetType> for Rank {
//     fn add_assign(&mut self, offset: OffsetType) {
//         *self = *self + offset
//     }
// }

#[derive(Debug, Copy, Clone, Eq, PartialEq, EnumCount, EnumIter, Hash)]
#[repr(i8)]
pub enum File {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
}

impl File {
    pub const fn to_index(&self) -> i8 {
        *self as i8
    }

    pub fn from_index(index: i8) -> Option<Self> {
        match index {
            0..=7 => unsafe { Some(Self::unsafe_from_index(index)) },
            _ => None,
        }
    }

    unsafe fn unsafe_from_index(index: i8) -> Self {
        debug_assert!(index >= 0);
        debug_assert!(index <= 7);
        mem::transmute(index)
    }
}

impl<T: Into<i8>> Add<T> for File {
    type Output = Result<Self, CoordinateArithmeticsError>;

    fn add(self, other: T) -> Result<Self, CoordinateArithmeticsError> {
        let self_index = self as i8; // Convert file to index (0...7)
        let offset = other.into();

        match self_index.checked_add(offset) {
            Some(result_index @ 0..=7) => unsafe { Ok(Self::unsafe_from_index(result_index)) },
            None => Err(CoordinateArithmeticsError::IntegerOverflowOccured),
            _ => Err(CoordinateArithmeticsError::ResultOutOfRange),
        }
    }
}

impl<OffsetType: Into<i8>> Sub<OffsetType> for File {
    type Output = Result<Self, CoordinateArithmeticsError>;

    fn sub(self, other: OffsetType) -> Result<Self, CoordinateArithmeticsError> {
        self + (-other.into())
    }
}

impl Sub<Self> for File {
    type Output = i8;

    fn sub(self, other: Self) -> i8 {
        self as i8 - other as i8
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, EnumCount, EnumIter, Hash)]
#[repr(i8)]
pub enum Square {
    A1,
    B1,
    C1,
    D1,
    E1,
    F1,
    G1,
    H1,
    A2,
    B2,
    C2,
    D2,
    E2,
    F2,
    G2,
    H2,
    A3,
    B3,
    C3,
    D3,
    E3,
    F3,
    G3,
    H3,
    A4,
    B4,
    C4,
    D4,
    E4,
    F4,
    G4,
    H4,
    A5,
    B5,
    C5,
    D5,
    E5,
    F5,
    G5,
    H5,
    A6,
    B6,
    C6,
    D6,
    E6,
    F6,
    G6,
    H6,
    A7,
    B7,
    C7,
    D7,
    E7,
    F7,
    G7,
    H7,
    A8,
    B8,
    C8,
    D8,
    E8,
    F8,
    G8,
    H8,
}

impl Square {
    pub fn new(file: File, rank: Rank) -> Square {
        unsafe { Square::from_index_unchecked(file.to_index() + (rank.to_index() << 3)) }
    }

    pub fn get_rank(self) -> Rank {
        unsafe { Rank::unsafe_from_index(self.to_index() >> 3) }
    }

    pub fn get_file(self) -> File {
        unsafe { File::unsafe_from_index(self.to_index() & 0b0000_0111) }
    }

    pub(super) const fn to_index(self) -> i8 {
        self as i8
    }

    pub fn from_index(index: i8) -> Option<Square> {
        if (0..64).contains(&index) {
            unsafe { Some(Self::from_index_unchecked(index)) }
        } else {
            None
        }
    }

    unsafe fn from_index_unchecked(index: i8) -> Square {
        debug_assert!((0..64).contains(&index));
        ::std::mem::transmute(index)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Vector {
    file_offset: i8,
    rank_offset: i8,
}

impl Vector {
    pub const NORTH: Vector = Vector::new(0, 1);
    pub const SOUTH: Vector = Vector::new(0, -1);
    pub const WEST: Vector = Vector::new(-1, 0);
    pub const EAST: Vector = Vector::new(1, 0);
    pub const NORTHWEST: Vector = Vector::new(-1, 1);
    pub const NORTHEAST: Vector = Vector::new(1, 1);
    pub const SOUTHWEST: Vector = Vector::new(-1, -1);
    pub const SOUTHEAST: Vector = Vector::new(1, -1);

    const fn new(file_offset: i8, rank_offset: i8) -> Self {
        Self {
            file_offset,
            rank_offset,
        }
    }

    pub fn get_relative_to_side(color: Color, vector: Vector) -> Vector {
        match color {
            Color::WHITE => vector,
            Color::BLACK => Vector::new(vector.file_offset, -vector.rank_offset),
        }
    }
}

impl const Mul<i8> for Vector {
    type Output = Self;

    fn mul(self, factor: i8) -> Self {
        Self::new(self.file_offset * factor, self.rank_offset * factor)
    }
}

impl const Mul<Vector> for i8 {
    type Output = Vector;

    fn mul(self, vector: Vector) -> Vector {
        vector * self
    }
}

impl const Add for Vector {
    type Output = Self;

    fn add(self, summand: Self) -> Self {
        Self::new(
            self.file_offset + summand.file_offset,
            self.rank_offset + summand.rank_offset,
        )
    }
}

impl Add<Vector> for Square {
    type Output = Result<Self, CoordinateArithmeticsError>;

    fn add(self, offset: Vector) -> Self::Output {
        Ok(Square::new(
            (self.get_file() + offset.file_offset)?,
            (self.get_rank() + offset.rank_offset)?,
        ))
    }
}

impl Add<Square> for Vector {
    type Output = Result<Square, CoordinateArithmeticsError>;

    fn add(self, square: Square) -> Self::Output {
        square + self
    }
}

impl Sub for Square {
    type Output = Vector;

    fn sub(self, other: Self) -> Vector {
        Vector::new(
            self.get_file() - other.get_file(),
            self.get_rank() - other.get_rank(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_squares() {
        assert_eq!(Square::B3, Square::B3);
        assert_ne!(Square::B3, Square::B4);
        assert_eq!(Some(Square::D2), Square::from_index(Square::D2.to_index()));
    }

    #[test]
    fn assert_representations() {
        assert_eq!(Rank::_1.to_index(), 0);
        assert_eq!(Rank::_8.to_index(), 7);
        assert_eq!(File::A.to_index(), 0);
        assert_eq!(File::H.to_index(), 7);
        assert_eq!(Square::A1.to_index(), 0);
        assert_eq!(Square::H1.to_index(), 7);
        assert_eq!(Square::H8.to_index(), 63);
    }
}
