// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

use super::square::Square;

use derive_more::IntoIterator;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[derive(Default)]
pub enum Promotion {
    #[default]
    NONE,
    KNIGHT,
    BISHOP,
    ROOK,
    QUEEN,
}



// TODO: Determine the most efficient move encoding (speed and size).
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Move {
    pub from: Square,
    pub to: Square,
    pub promotion: Promotion,
}

/// A wrapper type for `Vec<Move>`, so we can define Display on it.
/// This is similar to `MoveList`, but has a different semantic (a sequence of moves, that can be played sequentially)
/// instead of a set of moves out of which one can be played. Also `MoveList` might be implemented differently in the future.
#[derive(Default, Debug, Eq, PartialEq, Clone, IntoIterator)]
#[into_iterator(owned, ref, ref_mut)]
pub struct MoveSequence(pub Vec<Move>);

/// A set of moves, that can be played in a certain position.
#[derive(Default, Debug, Eq, PartialEq, Clone, IntoIterator)]
#[into_iterator(owned, ref, ref_mut)]
pub struct MoveSet(Vec<Move>);

impl MoveSet {
    pub fn add_move(&mut self, move_: Move) {
        if !self.contains(move_) {
            self.0.push(move_);
        }
    }

    pub fn contains(&self, move_: Move) -> bool {
        self.0.contains(&move_)
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn from_vec(vec: Vec<Move>) -> Self {
        Self(vec)
    }

    pub fn to_vec(self) -> Vec<Move> {
        self.0
    }

    pub fn first(&self) -> Option<&Move> {
        self.0.first()
    }
}

impl From<Vec<Move>> for MoveSequence {
    fn from(moves: Vec<Move>) -> MoveSequence {
        MoveSequence(moves)
    }
}

impl From<Vec<Move>> for MoveSet {
    fn from(moves: Vec<Move>) -> MoveSet {
        MoveSet(moves)
    }
}

impl std::ops::BitAnd<MoveSet> for MoveSet {
    type Output = Self;

    fn bitand(mut self, other: Self) -> Self {
        for move_ in other {
            self.add_move(move_)
        }

        self
    }
}

impl std::ops::BitAnd<&MoveSet> for &MoveSet {
    type Output = MoveSet;

    fn bitand(self, other: &MoveSet) -> MoveSet {
        self.clone() & other.clone()
    }
}
