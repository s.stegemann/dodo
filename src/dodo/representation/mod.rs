// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

pub mod bitboard;
pub mod boardstate;
pub mod gamestate;
pub mod r#move;
pub mod piece;
pub mod square;
pub mod zobrist;

pub use strum::IntoEnumIterator;

pub use self::bitboard::Bitboard;
pub use self::boardstate::{BoardState, EMPTY_BOARD, STARTING_POSITION_BOARD};
pub use self::gamestate::{CastlingRights, CastlingType, GameState};
pub use self::piece::{Color, Piece, PieceKind};
pub use self::r#move::{Move, MoveSequence, MoveSet, Promotion};
pub use self::square::{File, Rank, Square, Vector};
pub use self::zobrist::ZobristHash;
