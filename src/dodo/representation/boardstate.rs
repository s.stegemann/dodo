// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

use crate::representation::{Bitboard, Color, Piece, PieceKind, Square};

use enum_map::{enum_map, EnumMap};

#[derive(Copy, Clone, Eq, PartialEq, Default)]
pub struct BoardState {
    bitboards: EnumMap<Color, EnumMap<PieceKind, Bitboard>>,
}

impl BoardState {
    // pub const STARTING_POSITION: BoardState = Self {bitboards: [
    //     // White pieces
    //     [
    //         Bitboard::from_u64(0x00_00_00_00_00_00_ff_00), // Pawns
    //         Bitboard::from_u64(0x00_00_00_00_00_00_00_42), // Knights
    //         Bitboard::from_u64(0x00_00_00_00_00_00_00_24), // Bishops
    //         Bitboard::from_u64(0x00_00_00_00_00_00_00_81), // Rooks
    //         Bitboard::from_u64(0x00_00_00_00_00_00_00_08), // Queen
    //         Bitboard::from_u64(0x00_00_00_00_00_00_00_10), // King
    //     ],
    //     // Black pieces
    //     [
    //         Bitboard::from_u64(0x00_ff_00_00_00_00_00_00), // Pawns
    //         Bitboard::from_u64(0x42_00_00_00_00_00_00_00), // Knights
    //         Bitboard::from_u64(0x24_00_00_00_00_00_00_00), // Bishops
    //         Bitboard::from_u64(0x81_00_00_00_00_00_00_00), // Rooks
    //         Bitboard::from_u64(0x08_00_00_00_00_00_00_00), // Queen
    //         Bitboard::from_u64(0x10_00_00_00_00_00_00_00), // King
    //     ],
    // ]};

    pub fn get_piece_on_square(&self, square: Square) -> Option<Piece> {
        use crate::representation::square::strum::IntoEnumIterator;

        for color in Color::iter() {
            for kind in PieceKind::iter() {
                if self.bitboards[color][kind].is_set(square) {
                    return Some(Piece { color, kind });
                }
            }
        }

        None
    }

    pub fn get_color_on_square(&self, square: Square) -> Option<Color> {
        Some(self.get_piece_on_square(square)?.color)
    }

    pub fn get_piece_kind_on_square(&self, square: Square) -> Option<PieceKind> {
        Some(self.get_piece_on_square(square)?.kind)
    }

    pub fn get_positions(&self, piece: Piece) -> Bitboard {
        self.bitboards[piece.color][piece.kind]
    }

    pub fn get_king_position(&self, color: Color) -> Square {
        self.bitboards[color][PieceKind::KING]
            .into_iter()
            .next()
            .expect("There must be one king of each color.")
    }

    pub fn get_occupied_by_color(&self, color: Color) -> Bitboard {
        self.bitboards[color]
            .iter()
            .fold(Bitboard::EMPTY, |accumulator, (_, &bitboard)| {
                accumulator | bitboard
            })
    }

    pub fn get_occupied_by_piece_kind(&self, piece_kind: PieceKind) -> Bitboard {
        self.bitboards
            .iter()
            .fold(Bitboard::EMPTY, |accumulator, (_, &bitboards)| {
                accumulator | bitboards[piece_kind]
            })
    }

    pub fn get_occupied(&self) -> Bitboard {
        self.get_occupied_by_color(Color::WHITE) | self.get_occupied_by_color(Color::BLACK)
    }

    pub fn set_piece(&mut self, piece: Piece, square: Square) {
        assert!(
            self.get_piece_on_square(square).is_none(),
            "Trying to set piece {piece} on non-empty square {square}."
        );
        self.bitboards[piece.color][piece.kind].set(square)
    }

    pub fn unset_piece(&mut self, piece: Piece, square: Square) {
        assert!(
            self.get_positions(piece).is_set(square),
            "Trying to unset piece {piece} that is not on square {square}."
        );
        self.bitboards[piece.color][piece.kind].unset(square)
    }
}

lazy_static! {
    pub static ref STARTING_POSITION_BOARD: BoardState = BoardState {
        bitboards: enum_map! {
            Color::WHITE => enum_map!{
                PieceKind::PAWN   => Bitboard::from_squares(&[Square::A2, Square::B2, Square::C2, Square::D2, Square::E2, Square::F2, Square::G2, Square::H2]),
                PieceKind::KNIGHT => Bitboard::from_squares(&[Square::B1, Square::G1]),
                PieceKind::BISHOP => Bitboard::from_squares(&[Square::C1, Square::F1]),
                PieceKind::ROOK   => Bitboard::from_squares(&[Square::A1, Square::H1]),
                PieceKind::QUEEN  => Bitboard::from_squares(&[Square::D1]),
                PieceKind::KING   => Bitboard::from_squares(&[Square::E1]),
            },
            Color::BLACK => enum_map!{
                PieceKind::PAWN   => Bitboard::from_squares(&[Square::A7, Square::B7, Square::C7, Square::D7, Square::E7, Square::F7, Square::G7, Square::H7]),
                PieceKind::KNIGHT => Bitboard::from_squares(&[Square::B8, Square::G8]),
                PieceKind::BISHOP => Bitboard::from_squares(&[Square::C8, Square::F8]),
                PieceKind::ROOK   => Bitboard::from_squares(&[Square::A8, Square::H8]),
                PieceKind::QUEEN  => Bitboard::from_squares(&[Square::D8]),
                PieceKind::KING   => Bitboard::from_squares(&[Square::E8]),
            },
        },
    };
}

lazy_static! {
    pub static ref EMPTY_BOARD: BoardState = BoardState {
        bitboards: enum_map! {
            Color::WHITE => enum_map!{
                PieceKind::PAWN   => Bitboard::EMPTY,
                PieceKind::KNIGHT => Bitboard::EMPTY,
                PieceKind::BISHOP => Bitboard::EMPTY,
                PieceKind::ROOK   => Bitboard::EMPTY,
                PieceKind::QUEEN  => Bitboard::EMPTY,
                PieceKind::KING   => Bitboard::EMPTY,
            },
            Color::BLACK => enum_map!{
                PieceKind::PAWN   => Bitboard::EMPTY,
                PieceKind::KNIGHT => Bitboard::EMPTY,
                PieceKind::BISHOP => Bitboard::EMPTY,
                PieceKind::ROOK   => Bitboard::EMPTY,
                PieceKind::QUEEN  => Bitboard::EMPTY,
                PieceKind::KING   => Bitboard::EMPTY,
            },
        },
    };
}

impl std::ops::Index<Piece> for BoardState {
    type Output = Bitboard;

    fn index(&self, piece: Piece) -> &Bitboard {
        &self.bitboards[piece.color][piece.kind]
    }
}

impl std::ops::IndexMut<Piece> for BoardState {
    fn index_mut(&mut self, piece: Piece) -> &mut Bitboard {
        &mut self.bitboards[piece.color][piece.kind]
    }
}

impl std::ops::Index<Color> for BoardState {
    type Output = EnumMap<PieceKind, Bitboard>;

    fn index(&self, color: Color) -> &EnumMap<PieceKind, Bitboard> {
        &self.bitboards[color]
    }
}

impl std::ops::IndexMut<Color> for BoardState {
    fn index_mut(&mut self, color: Color) -> &mut EnumMap<PieceKind, Bitboard> {
        &mut self.bitboards[color]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn set_piece() {
        let mut boardstate = *EMPTY_BOARD;
        let piece = Piece {
            color: Color::WHITE,
            kind: PieceKind::KING,
        };

        boardstate.set_piece(piece, Square::E4);

        assert_eq!(boardstate.get_piece_on_square(Square::E4), Some(piece));
    }
}
