// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

extern crate strum;
extern crate strum_macros;

extern crate enum_map;

use enum_map::Enum;
use strum_macros::{EnumCount, EnumIter};

#[derive(Debug, Copy, Clone, Eq, PartialEq, EnumCount, EnumIter, Enum)]
pub enum Color {
    WHITE,
    BLACK,
}

impl Color {
    pub fn get_opponent(self) -> Color {
        match self {
            Color::WHITE => Color::BLACK,
            Color::BLACK => Color::WHITE,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, EnumCount, EnumIter, Enum)]
pub enum PieceKind {
    PAWN,
    KNIGHT,
    BISHOP,
    ROOK,
    QUEEN,
    KING,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Piece {
    pub color: Color,
    pub kind: PieceKind,
}

impl Piece {
    pub const fn new(color: Color, kind: PieceKind) -> Self {
        Piece { color, kind }
    }
}
