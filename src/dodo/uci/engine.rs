// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

use std::collections::HashMap;
use std::fs::File;
use std::io::{Read, Write};
use std::sync::mpsc::channel;
use std::sync::{Arc, Mutex};

use lazy_static::lazy_static;

use crate::built_info;
use crate::representation::GameState;
use crate::search::{SearchFinishedEvent, SearchManager, SearchUpdateEvent, TranspositionTable};
use crate::uci::{
    UciAnswer, UciCommand, UciIdVariant, UciInfo, UciOption, UciOptionCheck, UciOptionSpin,
    UciOptionString, UciReader, UciWriter,
};

#[derive(Debug, PartialEq, Eq)]
enum Mode {
    Waiting,
    Pondering,
    Searching,
}

impl Default for Mode {
    fn default() -> Self {
        Self::Waiting
    }
}

const DEFAULT_HASH_SIZE: i64 = 16;
const MIN_HASH_SIZE: i64 = 1;
const MAX_HASH_SIZE: i64 = 256 * 1024;

lazy_static! {
    static ref UCI_OPTIONS: HashMap<&'static str, Box<dyn UciOption>> = {
        let mut options = HashMap::<_, Box<dyn UciOption>>::new();

        options.insert(
            "Debug Log File",
            box UciOptionString::new("", |engine, value| {
                engine.debug_logfile = File::create(value).ok()
            }),
        );
        options.insert(
            "Hash",
            box UciOptionSpin::new(
                DEFAULT_HASH_SIZE,
                MIN_HASH_SIZE,
                MAX_HASH_SIZE,
                |engine, value| {
                    let mut transposition_table = engine.transposition_table.lock().unwrap();
                    transposition_table.resize(value);
                },
            ),
        );
        options.insert("Ponder", box UciOptionCheck::new(false, |_, _| ()));
        options.insert(
            "UCI_EngineAbout",
            box UciOptionString::new(built_info::PKG_DESCRIPTION, |_, _| ()),
        );

        options
    };
}

/// The main component of Dodo. An `Engine` can read commands via the UCI protocol and answer them.
///
/// This struct is responsible for starting and stopping the different search and communication
/// threads and coordinating the different parts of Dodo.
#[derive(Debug)]
pub struct Engine {
    debug_mode: bool,
    debug_logfile: Option<File>,
    searcher: SearchManager,
    history: Vec<GameState>,
    mode: Mode,
    transposition_table: Arc<Mutex<TranspositionTable>>,
}

impl Engine {
    pub fn run(input: impl Read, output: impl Write + Send) {
        let (uci_sender, uci_receiver) = channel::<UciAnswer>();

        let transposition_table = Arc::new(Mutex::new(TranspositionTable::new(DEFAULT_HASH_SIZE)));

        let update_sender = uci_sender.clone();
        let finished_sender = uci_sender.clone();

        let searcher = SearchManager::new(
            transposition_table.clone(),
            move |update_event: SearchUpdateEvent| {
                /* Update handler */
                if let Err(error) = update_sender.send(UciAnswer::Info(vec![
                    UciInfo::CurrMove(update_event.current_move),
                    UciInfo::Score(update_event.current_score),
                    UciInfo::Depth(update_event.depth),
                ])) {
                    eprintln!(
                        "An IO error occured while sending the search info to the GUI.: {error}"
                    );
                }
            },
            move |finished_event: SearchFinishedEvent| {
                /* Finished handler */
                if let Err(error) =
                    finished_sender.send(UciAnswer::BestMove(finished_event.best_move, None))
                {
                    eprintln!(
                        "An IO error occured while sending the best move to the GUI.: {error}"
                    );
                }
            },
        );

        let mut engine = Self {
            debug_mode: false,
            debug_logfile: None,
            searcher,
            history: vec![GameState::get_starting_position()],
            mode: Mode::Waiting,
            transposition_table,
        };

        let result = crossbeam::scope(|scope| {
            // Output thread.
            scope.spawn(move |_scope| {
                let mut writer = UciWriter::new(output);

                for answer in uci_receiver {
                    if let Err(error) = writer.send_answer(answer) {
                        eprintln!("An IO error occured while sending an UCI answer: {error}");
                        return;
                    }
                }
            });

            for command in UciReader::new(input) {
                match command {
                    Err(error) => {
                        eprintln!("An IO error occured while reading an UCI command: {error}")
                    }
                    Ok(command) => {
                        let (should_exit, answers) = engine.handle_command(command);
                        for answer in answers {
                            if let Err(error) = uci_sender.send(answer) {
                                eprintln!("An error has occured while sending an UCI answer to the output thread: {error}")
                            }
                        }
                        if should_exit {
                            break;
                        }
                    }
                }
            }

            // Drop every clone of uci_sender to close the channel and exit the output thread.
            drop(uci_sender);
            drop(engine);
        });

        if result.is_err() {
            eprintln!("An IO error occured in the output thread.")
        }
    }

    fn handle_command(&mut self, command: UciCommand) -> (bool, Vec<UciAnswer>) {
        (
            false,
            match command {
                UciCommand::Uci => {
                    let mut answers = vec![
                        UciAnswer::Id(UciIdVariant::Name, built_info::PKG_NAME.to_string()),
                        UciAnswer::Id(UciIdVariant::Author, built_info::PKG_AUTHORS.to_string()),
                    ];
                    for (name, option) in UCI_OPTIONS.iter() {
                        answers.push(UciAnswer::Option(name.to_string(), option.to_string()))
                    }
                    answers.push(UciAnswer::UciOk);

                    answers
                }
                UciCommand::Debug(new_value) => {
                    self.debug_mode = new_value;
                    vec![]
                }
                UciCommand::IsReady => vec![UciAnswer::ReadyOk],
                UciCommand::SetOption(name, value) => {
                    if let Some(option) = UCI_OPTIONS.get(name.as_str()) {
                        option.set_value(self, &value)
                    }
                    vec![]
                }
                UciCommand::UciNewGame => {
                    // Currently implemented as a noop. We might need to clear the hash table etc. on this command, if we implement those.
                    vec![]
                }
                UciCommand::Position(mut game_state, moves) => {
                    self.history = vec![game_state.clone()];

                    for move_ in moves {
                        use crate::search::MoveGenerator;

                        if !game_state.is_legal_move(move_) {
                            eprintln!("Received invalid move: {move_}");
                            break;
                        }
                        game_state.make_move_unchecked(move_);
                        self.history.push(game_state.clone());
                    }

                    vec![]
                }
                UciCommand::Go(search_options) => {
                    self.mode = if search_options.ponder {
                        Mode::Pondering
                    } else {
                        Mode::Searching
                    };

                    self.searcher.stop_searching();
                    self.searcher.start_searching(&self.history, search_options);

                    vec![]
                }
                UciCommand::Stop => {
                    if self.mode != Mode::Waiting {
                        self.searcher.stop_searching();
                        self.mode = Mode::Waiting;
                    }
                    vec![]
                }
                UciCommand::Ponderhit => {
                    if self.mode == Mode::Pondering {
                        self.mode = Mode::Searching
                    }
                    vec![]
                }
                UciCommand::Quit => {
                    self.searcher.stop_searching();
                    return (true, vec![]);
                }
            },
        )
    }
}
