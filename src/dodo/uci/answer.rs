// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

extern crate strum_macros;

use std::io::{LineWriter, Write};

use crate::evaluation::ScoreRange;
use crate::representation::{Move, MoveSequence};

use std::fmt::{self, Debug, Display, Formatter};

#[derive(Debug, strum_macros::ToString)]
#[strum(serialize_all = "lowercase")]
pub enum UciIdVariant {
    Name,
    Author,
}

/// Info the engine can send to the GUI in response to the a command.assert_eq!
/// Some commands like `go` will cause the engine to send multiple answers.assert_eq!
#[derive(Debug, strum_macros::ToString)]
#[strum(serialize_all = "lowercase")]
pub enum UciAnswer {
    Id(UciIdVariant, String),
    UciOk,
    ReadyOk,
    BestMove(Move, Option<Move>),
    // COPY_PROTECTION, // we don't need this option.
    // REGISTRATION, // we don't need this option.
    Info(Vec<UciInfo>),
    Option(String, String),
}

impl Display for UciAnswer {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        // Write the token for the answer type (equals the variant name).
        #[allow(clippy::recursive_format_impl, clippy::to_string_in_format_args)]
        write!(f, "{}", self.to_string())?;

        // Write additional data.
        match self {
            UciAnswer::Id(variant, value) => write!(f, " {} {}", variant.to_string(), value),
            UciAnswer::BestMove(bestmove, None) => write!(f, " {bestmove}"),
            UciAnswer::BestMove(bestmove, Some(pondermove)) => {
                write!(f, " {bestmove} ponder {pondermove}")
            }
            UciAnswer::Info(infos) => {
                for info in infos {
                    write!(f, " {info}")?
                }
                Ok(())
            }
            UciAnswer::Option(name, option) => write!(f, " name {name} {option}"),
            _ => Ok(()),
        }
    }
}

#[derive(Debug, strum_macros::ToString)]
#[strum(serialize_all = "lowercase")]
pub enum UciInfo {
    Depth(i32),
    SelDepth(i32),
    Time(i64),
    Nodes(i64),
    Pv(MoveSequence),
    MultiPv(i32),
    Score(ScoreRange),
    CurrMove(Move),
    CurrMoveNumber(i32),
    Hashfull(i32),
    Nps(i64),
    TbHits(i64),
    // SB_HITS(i64), // We don't support shredder endgame bases.
    CpuLoad(i32),
    Refutation(MoveSequence),
    CurrLine(i32, MoveSequence),
    String(String),
}

impl Display for UciInfo {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        // Write the token for the info type (equals the variant name).
        #[allow(clippy::recursive_format_impl, clippy::to_string_in_format_args)]
        write!(f, "{}", self.to_string())?;

        // Write additional data.
        match self {
            UciInfo::Depth(value) => write!(f, " {value}"),
            UciInfo::SelDepth(value) => write!(f, " {value}"),
            UciInfo::Time(value) => write!(f, " {value}"),
            UciInfo::Nodes(value) => write!(f, " {value}"),
            UciInfo::Pv(value) => write!(f, " {value}"),
            UciInfo::MultiPv(value) => write!(f, " {value}"),
            UciInfo::Score(value) => write!(f, " {value}"),
            UciInfo::CurrMove(value) => write!(f, " {value}"),
            UciInfo::CurrMoveNumber(value) => write!(f, " {value}"),
            UciInfo::Hashfull(value) => write!(f, " {value}"),
            UciInfo::Nps(value) => write!(f, " {value}"),
            UciInfo::TbHits(value) => write!(f, " {value}"),
            UciInfo::CpuLoad(value) => write!(f, " {value}"),
            UciInfo::Refutation(value) => write!(f, " {value}"),
            UciInfo::CurrLine(value1, value2) => write!(f, " {value1} {value2}"),
            UciInfo::String(value) => write!(f, " {value}"),
        }
    }
}

pub struct UciWriter<W: Write> {
    // Use a LineWriter instead of a BufWriter to ensure that every answer gets
    // flushed directly after is was written (every answer ends with a newline).
    writer: LineWriter<W>,
}

impl<W: Write> UciWriter<W> {
    pub fn new(writer: W) -> Self {
        Self {
            writer: LineWriter::new(writer),
        }
    }

    /// Sends an answer to the GUI.
    pub fn send_answer(&mut self, answer: UciAnswer) -> std::io::Result<()> {
        writeln!(self.writer, "{answer}")
    }
}

#[cfg(test)]
mod tests {
    use std::str;

    use strum_macros::{Display, EnumIter, EnumString};

    use crate::evaluation::Score;
    use crate::uci::{
        UciOptionButton, UciOptionCheck, UciOptionCombo, UciOptionSpin, UciOptionString,
    };

    use super::*;

    fn compare_serialized_answer(input: UciAnswer, expected: &str) {
        let mut writer = UciWriter::new(Vec::new());
        dbg!(&input);
        writer.send_answer(input).unwrap();
        let bytes = writer.writer.into_inner().unwrap();
        let output = str::from_utf8(&bytes).unwrap();

        assert_eq!(output, expected)
    }

    #[test]
    fn serialize_answer_id_name() {
        compare_serialized_answer(
            UciAnswer::Id(UciIdVariant::Name, "Dodo".to_string()),
            "id name Dodo\n",
        )
    }

    #[test]
    fn serialize_answer_id_author() {
        compare_serialized_answer(
            UciAnswer::Id(UciIdVariant::Author, "Sven Stegemann".to_string()),
            "id author Sven Stegemann\n",
        )
    }

    #[test]
    fn serialize_answer_uciok() {
        compare_serialized_answer(UciAnswer::UciOk, "uciok\n")
    }

    #[test]
    fn serialize_answer_readyok() {
        compare_serialized_answer(UciAnswer::ReadyOk, "readyok\n")
    }

    #[test]
    fn serialize_answer_bestmove_without_ponder() {
        compare_serialized_answer(
            UciAnswer::BestMove("e2e4".try_into().unwrap(), None),
            "bestmove e2e4\n",
        )
    }

    #[test]
    fn serialize_answer_bestmove_with_ponder() {
        compare_serialized_answer(
            UciAnswer::BestMove(
                "e1f2".try_into().unwrap(),
                Some("e7f8n".try_into().unwrap()),
            ),
            "bestmove e1f2 ponder e7f8n\n",
        )
    }

    #[test]
    fn serialize_answer_info_empty() {
        compare_serialized_answer(UciAnswer::Info(vec![]), "info\n")
    }

    #[test]
    fn serialize_answer_info_score() {
        compare_serialized_answer(
            UciAnswer::Info(vec![UciInfo::Score(ScoreRange::LowerBound(
                Score::Centipawns(-1234),
            ))]),
            "info score cp -1234 lowerbound\n",
        )
    }

    #[test]
    fn serialize_answer_info_score2() {
        compare_serialized_answer(
            UciAnswer::Info(vec![UciInfo::Score(ScoreRange::Exact(Score::MatingIn(
                123,
            )))]),
            "info score mate 123\n",
        )
    }

    #[test]
    fn serialize_answer_info_score3() {
        compare_serialized_answer(
            UciAnswer::Info(vec![UciInfo::Score(ScoreRange::UpperBound(
                Score::TablebaseLossIn(2),
            ))]),
            "info score cp -19998 upperbound\n",
        )
    }

    #[test]
    fn serialize_answer_info_everything() {
        compare_serialized_answer(
            UciAnswer::Info(vec!(
                UciInfo::Depth(1),
                UciInfo::SelDepth(2),
                UciInfo::Time(3),
                UciInfo::Nodes(4),
                UciInfo::Pv(vec![
                    "a1b2".try_into().unwrap(),
                    "h8g8".try_into().unwrap(),
                ].into()),
                UciInfo::MultiPv(5),
                UciInfo::Score(ScoreRange::UpperBound(Score::TablebaseLossIn(2))),
                UciInfo::CurrMove("e2e4".try_into().unwrap()),
                UciInfo::CurrMoveNumber(6),
                UciInfo::Hashfull(7),
                UciInfo::Nps(8),
                UciInfo::TbHits(9),
                UciInfo::CpuLoad(10),
                UciInfo::Refutation(vec![
                    "c2d1".try_into().unwrap(),
                    "h8a1".try_into().unwrap(),
                ].into()),
                UciInfo::CurrLine(11, vec![
                    "a8b7".try_into().unwrap(),
                    "c6d5".try_into().unwrap(),
                    "e4f3".try_into().unwrap(),
                    "g2h1".try_into().unwrap(),
                ].into()),
                UciInfo::String("Hallo Welt!".to_string()),
            )),
            "info depth 1 seldepth 2 time 3 nodes 4 pv a1b2 h8g8 multipv 5 score cp -19998 upperbound currmove e2e4 currmovenumber 6 hashfull 7 nps 8 tbhits 9 cpuload 10 refutation c2d1 h8a1 currline 11 a8b7 c6d5 e4f3 g2h1 string Hallo Welt!\n"
        )
    }

    #[test]
    fn serialize_answer_option_button() {
        compare_serialized_answer(
            UciAnswer::Option(
                "Clear Hash".to_string(),
                UciOptionButton::new(|_| ()).to_string(),
            ),
            "option name Clear Hash type button\n",
        )
    }

    #[test]
    fn serialize_answer_option_check() {
        compare_serialized_answer(
            UciAnswer::Option(
                "Nullmove".to_string(),
                UciOptionCheck::new(true, |_, _| ()).to_string(),
            ),
            "option name Nullmove type check default true\n",
        )
    }

    #[test]
    fn serialize_answer_option_combo() {
        #[derive(Debug, EnumString, EnumIter, Display)]
        enum Style {
            Solid,
            Normal,
            Risky,
        }

        compare_serialized_answer(
            UciAnswer::Option(
                "Style".to_string(),
                UciOptionCombo::new(Style::Normal, |_, _| ()).to_string(),
            ),
            "option name Style type combo default Normal var Solid var Normal var Risky\n",
        )
    }

    #[test]
    fn serialize_answer_option_spin() {
        compare_serialized_answer(
            UciAnswer::Option(
                "Selectivity".to_string(),
                UciOptionSpin::new(
                    2, // default
                    0, // min
                    4, // max
                    |_, _| (),
                )
                .to_string(),
            ),
            "option name Selectivity type spin default 2 min 0 max 4\n",
        )
    }

    #[test]
    fn serialize_answer_option_string() {
        compare_serialized_answer(
            UciAnswer::Option(
                "NalimovPath".to_string(),
                UciOptionString::new("c:\\", |_, _| ()).to_string(),
            ),
            "option name NalimovPath type string default c:\\\n",
        )
    }

    #[test]
    fn serialize_multiple_commands() {
        use indoc::indoc;

        let mut writer = UciWriter::new(Vec::new());
        let answers = vec![
            UciAnswer::UciOk,
            UciAnswer::BestMove("e2e4".try_into().unwrap(), None),
            UciAnswer::Info(vec![
                UciInfo::Pv(vec!["e2e4".try_into().unwrap(), "e7e5".try_into().unwrap()].into()),
                UciInfo::String("Hallo Welt!".to_string()),
            ]),
        ];

        for answer in answers {
            writer.send_answer(answer).unwrap();
        }
        let bytes = writer.writer.into_inner().unwrap();
        let output = str::from_utf8(&bytes).unwrap();

        assert_eq!(
            output,
            indoc!(
                "
            uciok
            bestmove e2e4
            info pv e2e4 e7e5 string Hallo Welt!
        "
            )
        )
    }
}
