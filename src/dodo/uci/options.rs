// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

use std::fmt::{self, Debug, Display, Formatter};
use std::str::FromStr;

use derive_more::Constructor;
use strum::IntoEnumIterator;

use crate::uci::Engine;

pub trait UciOption: Display + Sync {
    fn set_value(&self, engine: &mut Engine, value: &str);
}

impl Debug for dyn UciOption {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "UciOption(\"{self}\")")
    }
}

#[derive(Constructor, Debug)]
pub struct UciOptionCheck<Handler: FnMut(&mut Engine, bool)> {
    default: bool,
    handler: Handler,
}

impl<Handler: FnMut(&mut Engine, bool)> Display for UciOptionCheck<Handler> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "type check default {}", self.default)
    }
}

impl<Handler: FnMut(&mut Engine, bool) + Sync + Clone> UciOption for UciOptionCheck<Handler> {
    fn set_value(&self, engine: &mut Engine, value: &str) {
        if let Ok(value) = value.parse() {
            (self.handler.clone())(engine, value)
        }
    }
}

#[derive(Constructor, Debug)]
pub struct UciOptionSpin<Handler: FnMut(&mut Engine, i64)> {
    default: i64,
    min: i64,
    max: i64,
    handler: Handler,
}

impl<Handler: FnMut(&mut Engine, i64)> Display for UciOptionSpin<Handler> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "type spin default {} min {} max {}",
            self.default, self.min, self.max
        )
    }
}

impl<Handler: FnMut(&mut Engine, i64) + Sync + Clone> UciOption for UciOptionSpin<Handler> {
    fn set_value(&self, engine: &mut Engine, value: &str) {
        if let Ok(value) = value.parse() {
            if (self.min..=self.max).contains(&value) {
                (self.handler.clone())(engine, value)
            }
        }
    }
}

#[derive(Constructor, Debug)]
pub struct UciOptionCombo<
    Variant: Display + IntoEnumIterator + FromStr,
    Handler: FnMut(&mut Engine, Variant),
> {
    default: Variant,
    handler: Handler,
}

impl<Variant: Display + IntoEnumIterator + FromStr, Handler: FnMut(&mut Engine, Variant)> Display
    for UciOptionCombo<Variant, Handler>
{
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "type combo default {}", self.default)?;
        for variant in Variant::iter() {
            write!(f, " var {variant}")?;
        }
        Ok(())
    }
}

impl<
        Variant: Display + IntoEnumIterator + FromStr + Sync,
        Handler: FnMut(&mut Engine, Variant) + Sync + Clone,
    > UciOption for UciOptionCombo<Variant, Handler>
{
    fn set_value(&self, engine: &mut Engine, value: &str) {
        if let Ok(value) = value.parse() {
            (self.handler.clone())(engine, value)
        }
    }
}

#[derive(Constructor, Debug)]
pub struct UciOptionButton<Handler: FnMut(&mut Engine)> {
    handler: Handler,
}

impl<Handler: FnMut(&mut Engine)> Display for UciOptionButton<Handler> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "type button")
    }
}

impl<Handler: FnMut(&mut Engine) + Sync + Clone> UciOption for UciOptionButton<Handler> {
    fn set_value(&self, engine: &mut Engine, _value: &str) {
        (self.handler.clone())(engine)
    }
}

#[derive(Constructor, Debug)]
pub struct UciOptionString<'a, Handler: FnMut(&mut Engine, &str)> {
    default: &'a str,
    handler: Handler,
}

impl<Handler: FnMut(&mut Engine, &str)> Display for UciOptionString<'_, Handler> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "type string default {}", self.default)
    }
}

impl<Handler: FnMut(&mut Engine, &str) + Sync + Clone> UciOption for UciOptionString<'_, Handler> {
    fn set_value(&self, engine: &mut Engine, value: &str) {
        (self.handler.clone())(engine, value)
    }
}
