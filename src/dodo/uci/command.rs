// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

extern crate strum_macros;

use std::fmt::Debug;
use std::io::{BufRead, BufReader, Lines, Read};
use std::str::{FromStr, SplitWhitespace};
use std::time::{Duration, Instant};

use enum_map::enum_map;
use strum::VariantNames;
use strum_macros::EnumVariantNames;

use crate::parsing::FenConvertable;
use crate::representation::{Color, GameState, Move, MoveSequence, MoveSet};
use crate::search::{ClockState, ClockStateForSide, SearchLimit, SearchOptions};

// A command that the GUI can send to the engine.
#[derive(Debug, strum_macros::ToString, EnumVariantNames, Eq, PartialEq)]
#[strum(serialize_all = "lowercase")]
pub enum UciCommand {
    Uci,
    Debug(bool),
    IsReady,
    SetOption(String, String),
    // Register, // Does not apply to us, so just ignore this command.
    UciNewGame,
    Position(GameState, MoveSequence),
    Go(SearchOptions),
    Stop,
    Ponderhit,
    Quit,
}

pub struct UciReader<R: Read> {
    lines: Lines<BufReader<R>>,
}

trait FromUci<T> {
    fn try_parse_uci(&mut self) -> Option<T>;
}

// We can parse most fields in UCI using str::parse().
// Specializations for the other fields are below.
impl<T: FromStr> FromUci<T> for SplitWhitespace<'_> {
    default fn try_parse_uci(&mut self) -> Option<T> {
        self.next()?.parse().ok()
    }
}

impl FromUci<MoveSequence> for SplitWhitespace<'_> {
    fn try_parse_uci(&mut self) -> Option<MoveSequence> {
        Some(try_parse_moves(self)?.into())
    }
}

impl FromUci<MoveSet> for SplitWhitespace<'_> {
    fn try_parse_uci(&mut self) -> Option<MoveSet> {
        Some(try_parse_moves(self)?.into())
    }
}

impl FromUci<GameState> for SplitWhitespace<'_> {
    fn try_parse_uci(&mut self) -> Option<GameState> {
        use itertools::Itertools;

        match self.next()? {
            "fen" => GameState::from_fen(&self.take(6).join(" ")),
            "startpos" => Some(GameState::get_starting_position()),
            _ => None,
        }
    }
}

impl FromUci<SearchOptions> for SplitWhitespace<'_> {
    fn try_parse_uci(&mut self) -> Option<SearchOptions> {
        let mut result = SearchOptions::default();
        let now = Instant::now();
        let (mut wtime, mut btime, mut winc, mut binc, mut movestogo) = (0, 0, 0, 0, 0);

        while let Some(token) = self.next() {
            match token {
                "searchmoves" => result.search_moves = Some(self.try_parse_uci()?),
                "ponder" => result.ponder = true,
                "wtime" => wtime = self.try_parse_uci()?,
                "btime" => btime = self.try_parse_uci()?,
                "winc" => winc = self.try_parse_uci()?,
                "binc" => binc = self.try_parse_uci()?,
                "movestogo" => movestogo = self.try_parse_uci()?,
                "depth" => result
                    .search_limits
                    .push(SearchLimit::Depth(self.try_parse_uci()?)),
                "nodes" => result
                    .search_limits
                    .push(SearchLimit::Nodes(self.try_parse_uci()?)),
                "mate" => result
                    .search_limits
                    .push(SearchLimit::Mate(self.try_parse_uci()?)),
                "movetime" => result.search_limits.push(SearchLimit::Time(
                    now + Duration::from_millis(self.try_parse_uci()?),
                )),
                "infinite" => result.search_limits.push(SearchLimit::Infinite),
                _ => {} // Ignore unknown search options according to UCI spec.
            }
        }

        if wtime != 0 || btime != 0 {
            // Use time management
            let white_clock = if wtime == 0 {
                None
            } else {
                Some(ClockStateForSide {
                    clock_ends: now + Duration::from_millis(wtime),
                    increment: Duration::from_millis(winc),
                })
            };

            let black_clock = if btime == 0 {
                None
            } else {
                Some(ClockStateForSide {
                    clock_ends: now + Duration::from_millis(btime),
                    increment: Duration::from_millis(binc),
                })
            };

            result.search_limits.push(SearchLimit::Clock(ClockState {
                clocks: enum_map!(
                    Color::WHITE => white_clock,
                    Color::BLACK => black_clock,
                ),
                moves_to_go: if movestogo == 0 {
                    None
                } else {
                    Some(movestogo)
                },
            }))
        }

        Some(result)
    }
}

// The debug command takes "on" or "off" as a parameter instead of "true" or "false".
fn try_parse_on_off(tokens: &mut SplitWhitespace) -> Option<bool> {
    match tokens.next()? {
        "on" => Some(true),
        "off" => Some(false),
        _ => None,
    }
}

fn try_parse_moves(tokens: &mut SplitWhitespace) -> Option<Vec<Move>> {
    let mut result: Vec<Move> = vec![];

    // Pretty ugly hack:
    // We clone `tokens`, to prevent this function from consuming the token after the last valid move.
    // The parsed moves will not be consumed either and be skipped by the rest of the parse code,
    // because moves do not match any of the UCI keywords.
    // TODO: Is there a better way?
    for token in tokens.clone() {
        match token.try_into() {
            Ok(move_) => result.push(move_),
            _ => return Some(result),
        }
    }

    Some(result)
}

impl<R: Read> UciReader<R> {
    pub fn new(reader: R) -> Self {
        UciReader {
            lines: BufReader::new(reader).lines(),
        }
    }

    fn parse_set_option(mut tokens: SplitWhitespace) -> Option<UciCommand> {
        let mut name = String::new();
        let mut value = String::new();

        while let Some(token) = tokens.next() {
            if token == "name" {
                name = tokens.next()?.to_string();
                while let Some(token) = tokens.next() {
                    if token != "value" {
                        name += " ";
                        name += token;
                    } else {
                        value = tokens.next()?.to_string();
                        for token in &mut tokens {
                            value += " ";
                            value += token;
                        }
                    }
                }
            }
        }

        if name.is_empty() {
            return None;
        }

        Some(UciCommand::SetOption(name, value))
    }

    /// Reads a command from an token iterator.
    fn parse_command(mut tokens: SplitWhitespace) -> Option<UciCommand> {
        // UCI spec:
        //
        // > if the engine or the GUI receives an unknown command or token it should just ignore it and try to
        // > parse the rest of the string in this line.
        // > Examples: "joho debug on\n" should switch the debug mode on given that joho is not defined,
        // >           "debug joho on\n" will be undefined however.

        // Read until a valid command name was found.
        let mut command_name = tokens.next()?;
        while !UciCommand::VARIANTS.contains(&command_name) {
            command_name = tokens.next()?
        }

        Some(match command_name {
            "uci" => UciCommand::Uci,
            "debug" => UciCommand::Debug(try_parse_on_off(&mut tokens)?),
            "setoption" => Self::parse_set_option(tokens)?,
            "isready" => UciCommand::IsReady,
            "ucinewgame" => UciCommand::UciNewGame,
            "position" => {
                let position = tokens.try_parse_uci()?;

                // Ignore tokens until "moves" is found or tokens is empty.
                for token in &mut tokens {
                    if token == "moves" {
                        break;
                    }
                }

                UciCommand::Position(position, tokens.try_parse_uci()?)
            }
            "go" => UciCommand::Go(tokens.try_parse_uci()?),
            "stop" => UciCommand::Stop,
            "ponderhit" => UciCommand::Ponderhit,
            "quit" => UciCommand::Quit,
            _ => unreachable!(),
        })
    }

    /// Blocks until a command from the GUI was read.
    /// Returns None if there are no more commands (EOF) and Some(io::Err) if there was a IO error.
    pub fn wait_for_command(&mut self) -> Option<std::io::Result<UciCommand>> {
        // The UCI spec says we have to ignore invalid commands.
        // Read lines until a valid command, an IO error or EOF is found.
        loop {
            // Check for an early return.
            let line = match self.lines.next() {
                None => return None,                         // End of file.
                Some(Err(error)) => return Some(Err(error)), // Pass IO errors to caller.
                Some(Ok(line)) => line,                      // Continue parsing this line.
            };

            // UCI spec: "arbitrary white space between tokens is allowed"
            // Let's interpret this to mean Unicode whitespace.
            if let Some(command) = Self::parse_command(line.split_whitespace()) {
                return Some(Ok(command));
            }
        }
    }
}

impl<R: Read> Iterator for UciReader<R> {
    type Item = std::io::Result<UciCommand>;

    fn next(&mut self) -> Option<std::io::Result<UciCommand>> {
        self.wait_for_command()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn compare_deserialized_command(command: &str, expected: UciCommand) {
        let mut reader = UciReader::new(command.as_bytes());

        let output = reader.wait_for_command().unwrap().unwrap();

        assert_eq!(output, expected);
    }

    #[test]
    fn deserialize_command_uci() {
        compare_deserialized_command("uci\n", UciCommand::Uci);
    }

    #[test]
    fn deserialize_command_debug() {
        compare_deserialized_command("debug on\n", UciCommand::Debug(true));
    }

    #[test]
    fn deserialize_command_isready() {
        compare_deserialized_command("isready\n", UciCommand::IsReady);
    }

    #[test]
    fn deserialize_command_setoption() {
        compare_deserialized_command(
            "setoption name Nullmove value true\n",
            UciCommand::SetOption("Nullmove".to_string(), "true".to_string()),
        );
    }

    #[test]
    fn deserialize_command_setoption_button() {
        compare_deserialized_command(
            "setoption name Clear Hash\n",
            UciCommand::SetOption("Clear Hash".to_string(), "".to_string()),
        );
    }

    #[test]
    fn deserialize_command_ucinewgame() {
        compare_deserialized_command("ucinewgame\n", UciCommand::UciNewGame);
    }

    #[test]
    fn deserialize_command_position() {
        compare_deserialized_command(
            "position startpos\n",
            UciCommand::Position(GameState::get_starting_position(), MoveSequence::default()),
        );
    }

    #[test]
    fn deserialize_command_position_with_moves() {
        compare_deserialized_command(
            "position startpos moves e2e4 e7e5\n",
            UciCommand::Position(
                GameState::get_starting_position(),
                vec!["e2e4".try_into().unwrap(), "e7e5".try_into().unwrap()].into(),
            ),
        );
    }

    #[test]
    fn deserialize_command_position_with_fen_and_moves() {
        compare_deserialized_command(
            "position fen 3k4/8/8/8/8/8/4Q3/3K4 w - - 0 1 moves e2e7 d8c7\n",
            UciCommand::Position(
                GameState::from_fen("3k4/8/8/8/8/8/4Q3/3K4 w - - 0 1").unwrap(),
                vec!["e2e7".try_into().unwrap(), "d8c7".try_into().unwrap()].into(),
            ),
        );
    }

    #[test]
    fn deserialize_command_go_infinite() {
        compare_deserialized_command(
            "go infinite\n",
            UciCommand::Go(SearchOptions {
                search_moves: None,
                ponder: false,
                search_limits: vec![SearchLimit::Infinite],
            }),
        );
    }

    #[test]
    fn deserialize_command_go_ponder() {
        compare_deserialized_command(
            "go ponder\n",
            UciCommand::Go(SearchOptions {
                search_moves: None,
                ponder: true,
                search_limits: vec![],
            }),
        );
    }

    #[test]
    fn deserialize_command_go_mate() {
        compare_deserialized_command(
            "go mate 123\n",
            UciCommand::Go(SearchOptions {
                search_moves: None,
                ponder: false,
                search_limits: vec![SearchLimit::Mate(123)],
            }),
        );
    }

    #[test]
    fn deserialize_command_go_searchmoves() {
        compare_deserialized_command(
            "go searchmoves e2e4 d2d4\n",
            UciCommand::Go(SearchOptions {
                search_moves: Some(
                    vec!["e2e4".try_into().unwrap(), "d2d4".try_into().unwrap()].into(),
                ),
                ponder: false,
                search_limits: vec![],
            }),
        );
    }

    #[test]
    fn deserialize_command_go_time_management() {
        let mut reader = UciReader::new(
            "go wtime 10000 btime 20000 winc 100 binc 200 movestogo 12\n".as_bytes(),
        );

        let output = reader.wait_for_command().unwrap().unwrap();
        if let UciCommand::Go(search_options) = output {
            assert_eq!(search_options.search_moves, None);
            assert!(!search_options.ponder);

            if let SearchLimit::Clock(clock_state) = search_options.search_limits.first().unwrap() {
                // Can't test clock_state_clocks[...].unwrap().clock_ends, because we don't know
                // what Instant::now() has returned.
                assert_eq!(
                    clock_state.clocks[Color::WHITE]
                        .unwrap()
                        .increment
                        .as_millis(),
                    100
                );
                assert_eq!(
                    clock_state.clocks[Color::BLACK]
                        .unwrap()
                        .increment
                        .as_millis(),
                    200
                );
                assert_eq!(clock_state.moves_to_go, Some(12));
            } else {
                panic!()
            }
        } else {
            panic!()
        }
    }

    #[test]
    fn deserialize_command_stop() {
        compare_deserialized_command("stop\n", UciCommand::Stop);
    }

    #[test]
    fn deserialize_command_ponderhit() {
        compare_deserialized_command("ponderhit\n", UciCommand::Ponderhit);
    }

    #[test]
    fn deserialize_command_quit() {
        compare_deserialized_command("quit\n", UciCommand::Quit);
    }

    #[test]
    fn deserialize_multiple_commands() {
        use indoc::indoc;

        // The example from the UCI spec:
        let mut reader = UciReader::new(
            indoc!(
                "
            uci
            setoption name Hash value 32
            setoption name NalimovCache value 1
            setoption name NalimovPath value d:\\tb;c\\tb
            isready
            ucinewgame
            setoption name UCI_AnalyseMode value true
            position startpos moves e2e4 e7e5
            go infinite
            stop
        "
            )
            .as_bytes(),
        );

        let expected_commands = [
            UciCommand::Uci,
            UciCommand::SetOption("Hash".to_string(), "32".to_string()),
            UciCommand::SetOption("NalimovCache".to_string(), "1".to_string()),
            UciCommand::SetOption("NalimovPath".to_string(), "d:\\tb;c\\tb".to_string()),
            UciCommand::IsReady,
            UciCommand::UciNewGame,
            UciCommand::SetOption("UCI_AnalyseMode".to_string(), "true".to_string()),
            UciCommand::Position(
                GameState::get_starting_position(),
                vec!["e2e4".try_into().unwrap(), "e7e5".try_into().unwrap()].into(),
            ),
            UciCommand::Go(SearchOptions {
                search_moves: None,
                ponder: false,
                search_limits: vec![SearchLimit::Infinite],
            }),
            UciCommand::Stop,
        ];

        for expected_command in expected_commands {
            let output = reader.wait_for_command().unwrap().unwrap();
            assert_eq!(output, expected_command)
        }
    }
}
