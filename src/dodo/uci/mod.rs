// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

pub mod answer;
pub mod command;
pub mod engine;
pub mod options;

pub use answer::{UciAnswer, UciIdVariant, UciInfo, UciWriter};
pub use command::{UciCommand, UciReader};
pub use engine::Engine;
pub use options::{
    UciOption, UciOptionButton, UciOptionCheck, UciOptionCombo, UciOptionSpin, UciOptionString,
};
