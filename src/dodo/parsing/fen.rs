// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

extern crate enum_map;
extern crate itertools;
extern crate regex;
extern crate strum;

use crate::representation::GameState;

pub trait FenConvertable {
    fn from_fen(fen: &str) -> Option<GameState>;
    fn to_fen(self) -> String;
}

impl FenConvertable for GameState {
    fn from_fen(fen: &str) -> Option<GameState> {
        use crate::representation::{
            CastlingRights, CastlingType, Color, File, Piece, Rank, Square, EMPTY_BOARD,
        };
        use regex::Regex;

        let mut board = *EMPTY_BOARD;

        let mut fen_parts = fen.trim().split(' ');

        // Parse piece placement.

        let mut ranks_parsed: i8 = 0;
        let mut files_parsed_in_rank: i8 = 0;

        for char in fen_parts.next()?.chars() {
            match char {
                // Rank delimiter
                '/' => {
                    if files_parsed_in_rank != 8 {
                        return None;
                    }

                    ranks_parsed += 1;
                    files_parsed_in_rank = 0;
                }

                // Digit (number of empty squares)
                digit @ '1'..='8' => {
                    files_parsed_in_rank += digit.to_digit(10)? as i8;
                }

                // Piece
                piece => {
                    board.set_piece(
                        Piece::try_from(piece).ok()?,
                        Square::new(
                            File::from_index(files_parsed_in_rank)?,
                            Rank::from_index(7 - ranks_parsed)?,
                        ),
                    );
                    files_parsed_in_rank += 1;
                }
            }

            // Check coordinates in range
            if ranks_parsed > 8 || files_parsed_in_rank > 8 {
                return None;
            }
        }

        // Ensure, that we have read 7 complete ranks and 8 files in the last rank
        // The last rank does not end with an slash character and does therefore not increase ranks_parsed.
        if ranks_parsed != 7 || files_parsed_in_rank != 8 {
            return None;
        }

        // Parse side to move.

        let side_to_move = match fen_parts.next()? {
            "w" => Color::WHITE,
            "b" => Color::BLACK,
            _ => return None,
        };

        // Parse castling rights.

        let castling_rights_string = fen_parts.next()?;
        if castling_rights_string.is_empty()
            || !Regex::new(r"K?Q?k?q?|-")
                .unwrap()
                .is_match(castling_rights_string)
        {
            return None;
        };

        let mut castling_rights = CastlingRights::default();
        for char in castling_rights_string.chars() {
            match char {
                'K' => castling_rights[Color::WHITE][CastlingType::KINGSIDE] = true,
                'Q' => castling_rights[Color::WHITE][CastlingType::QUEENSIDE] = true,
                'k' => castling_rights[Color::BLACK][CastlingType::KINGSIDE] = true,
                'q' => castling_rights[Color::BLACK][CastlingType::QUEENSIDE] = true,
                '-' => (),
                _ => return None,
            }
        }

        // Parse en passant target square.

        let en_passant_square = match fen_parts.next()? {
            "-" => None,
            square => Some(Square::try_from(square).ok()?),
        };

        // Parse halfmove clock.

        let fifty_move_counter: i8 = fen_parts.next()?.parse().ok()?;
        if fifty_move_counter < 0 {
            return None;
        }

        // Parse fullmove number (gets ignored)

        let fullmove_number: i8 = fen_parts.next()?.parse().ok()?;
        if fullmove_number < 0 {
            return None;
        }

        // No parsing error occured. Return parsed game state.
        Some(GameState::new(
            board,
            side_to_move,
            castling_rights,
            en_passant_square,
            fifty_move_counter,
        ))
    }

    fn to_fen(self) -> String {
        use crate::representation::square::strum::IntoEnumIterator;
        use crate::representation::{CastlingType, Color, File, Rank, Square};
        use itertools::Itertools;
        use std::char;

        let board = Rank::iter()
            .map(|rank| {
                let mut rank_string = "".to_string();
                let mut consecutive_empty_squares = 0;

                for file in File::iter() {
                    match self
                        .get_board()
                        .get_piece_on_square(Square::new(file, rank))
                    {
                        None => {
                            consecutive_empty_squares += 1;
                        }
                        Some(piece) => {
                            if consecutive_empty_squares > 0 {
                                rank_string
                                    .push(char::from_digit(consecutive_empty_squares, 10).unwrap());
                            }
                            rank_string.push(piece.to_letter_symbol());
                            consecutive_empty_squares = 0;
                        }
                    }
                }

                if consecutive_empty_squares > 0 {
                    rank_string.push(char::from_digit(consecutive_empty_squares, 10).unwrap());
                }

                rank_string
            })
            .collect::<Vec<_>>()
            .iter()
            .rev()
            .join("/");

        let side_to_move = match self.get_side_to_move() {
            Color::WHITE => 'w',
            Color::BLACK => 'b',
        };

        let mut castling_rights = "".to_string();
        if self.can_castle_to_side(Color::WHITE, CastlingType::KINGSIDE) {
            castling_rights.push('K')
        }
        if self.can_castle_to_side(Color::WHITE, CastlingType::QUEENSIDE) {
            castling_rights.push('Q')
        }
        if self.can_castle_to_side(Color::BLACK, CastlingType::KINGSIDE) {
            castling_rights.push('k')
        }
        if self.can_castle_to_side(Color::BLACK, CastlingType::QUEENSIDE) {
            castling_rights.push('q')
        }
        if castling_rights.is_empty() {
            castling_rights = "-".to_string()
        }

        let en_passant_square = self
            .get_en_passant_square()
            .map_or("-".to_string(), |square| square.to_string());

        let fifty_move_counter = self.get_fifty_move_counter();

        // Always act like this is the first move, because we do not save the move number.
        let fullmove_number = "1";

        format!(
            "{board} {side_to_move} {castling_rights} {en_passant_square} {fifty_move_counter} {fullmove_number}"
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fen_to_gamestate_starting_position() {
        let fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

        assert_eq!(
            GameState::from_fen(fen).unwrap(),
            GameState::get_starting_position()
        );
    }

    #[test]
    fn starting_position_to_fen_and_back() {
        assert_eq!(
            GameState::from_fen(GameState::get_starting_position().to_fen().as_str()),
            Some(GameState::get_starting_position())
        );
    }

    #[test]
    fn fen_to_gamestate_and_back() {
        let fen = "r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3/q5b1 b - - 1 1";

        assert_eq!(GameState::from_fen(fen).unwrap().to_fen(), fen);
    }
}
