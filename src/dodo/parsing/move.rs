// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019-2021 Sven Stegemann

use std::convert::TryFrom;
use std::fmt::{self, Display, Formatter};

use crate::parsing::ChessParseError;
use crate::representation::{
    File, GameState, Move, MoveSequence, MoveSet, Piece, PieceKind, Promotion, Rank, Square,
};
use crate::search::MoveGenerator;

impl TryFrom<&str> for Promotion {
    type Error = ChessParseError;

    fn try_from(string: &str) -> Result<Promotion, Self::Error> {
        match string.to_ascii_lowercase().as_str() {
            "" => Ok(Promotion::NONE),
            "n" => Ok(Promotion::KNIGHT),
            "b" => Ok(Promotion::BISHOP),
            "r" => Ok(Promotion::ROOK),
            "q" => Ok(Promotion::QUEEN),
            _ => Err(ChessParseError()),
        }
    }
}

impl TryFrom<char> for Promotion {
    type Error = ChessParseError;

    fn try_from(character: char) -> Result<Promotion, Self::Error> {
        character.to_string().as_str().try_into()
    }
}

impl TryFrom<&str> for Move {
    type Error = ChessParseError;

    fn try_from(string: &str) -> Result<Move, Self::Error> {
        if string.len() < 4 || string.len() > 5 {
            return Err(ChessParseError());
        }

        if !string.is_ascii() {
            return Err(ChessParseError());
        }

        Ok(Move {
            from: string[0..2].try_into()?,
            to: string[2..4].try_into()?,
            promotion: string[4..].try_into()?,
        })
    }
}

pub trait SanParsable
where
    Self: Sized,
{
    // Parses a move from SAN (short algebraic notation) and checks if the move is legal.
    fn from_san(string: &str, game_state: GameState) -> Result<Self, ChessParseError>;
}

impl SanParsable for Move {
    fn from_san(string: &str, game_state: GameState) -> Result<Move, ChessParseError> {
        // <sanMove>        ::= <PieceCode> [<Disambiguation>] <targetSquare> [<promotion>] ['+'|'#']
        // | <castles>
        // <castles>        ::= 'O-O' | 'O-O-O' (upper case O, not zero)
        // <PieceCode>      ::= '' | 'N' | 'B' | 'R' | 'Q' | 'K'
        // <Disambiguation> ::= <fileLetter> | <digit18>
        // <targetSquare>   ::= <fileLetter> <digit18>
        // <fileLetter> ::= 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h'
        // <promotion>      ::=  '=' <PiecePromotion>
        // <PiecePromotion> ::= 'N' | 'B' | 'R' | 'Q'

        let side_to_move = game_state.get_side_to_move();
        let castling_rank = Rank::get_relative_to_side(side_to_move, Rank::_1);

        if string == "0-0" {
            let result = Move {
                from: Square::new(File::E, castling_rank),
                to: Square::new(File::G, castling_rank),
                promotion: Promotion::NONE,
            };

            return if game_state.is_legal_move(result) {
                Ok(result)
            } else {
                Err(ChessParseError())
            };
        }

        if string == "0-0-0" {
            let result = Move {
                from: Square::new(File::E, castling_rank),
                to: Square::new(File::C, castling_rank),
                promotion: Promotion::NONE,
            };

            return if game_state.is_legal_move(result) {
                Ok(result)
            } else {
                Err(ChessParseError())
            };
        }

        let mut chars = string.chars().peekable();

        let piece_kind = match chars.peek().ok_or(ChessParseError())? {
            'N' | 'B' | 'R' | 'Q' => chars.next().unwrap().try_into().unwrap(),
            _ => PieceKind::PAWN,
        };

        let file1: Option<File> = match chars.peek() {
            Some('a'..='h') => chars.next().unwrap().try_into().ok(),
            _ => None,
        };
        let rank1: Option<Rank> = match chars.peek() {
            Some('1'..='8') => chars.next().unwrap().try_into().ok(),
            _ => None,
        };
        let file2: Option<File> = match chars.peek() {
            Some('a'..='h') => chars.next().unwrap().try_into().ok(),
            _ => None,
        };
        let rank2: Option<Rank> = match chars.peek() {
            Some('1'..='8') => chars.next().unwrap().try_into().ok(),
            _ => None,
        };

        let disambiguation = rank2.is_some() || file2.is_some();

        let (disambiguation_file, disambiguation_rank) = if disambiguation {
            (file1, rank1)
        } else {
            (None, None)
        };

        let destination_square = if disambiguation {
            Square::new(
                file2.ok_or(ChessParseError())?,
                rank2.ok_or(ChessParseError())?,
            )
        } else {
            Square::new(
                file1.ok_or(ChessParseError())?,
                rank1.ok_or(ChessParseError())?,
            )
        };

        let promotion: Promotion = if chars.next() == Some('=') {
            chars.next().ok_or(ChessParseError())?.try_into()?
        } else {
            Promotion::NONE
        };

        // Ensure that the rest of the string is valid i.e. matches ['+'|'#'].
        match chars.next() {
            None | Some('+' | '#') => (),
            _ => return Err(ChessParseError()),
        }
        if chars.next().is_some() {
            return Err(ChessParseError());
        }

        // Find origin square.
        let piece = Piece::new(side_to_move, piece_kind);
        let mut potential_origin_squares = game_state.get_board().get_positions(piece);
        if let Some(disambiguation_file) = disambiguation_file {
            potential_origin_squares &= disambiguation_file;
        }
        if let Some(disambiguation_rank) = disambiguation_rank {
            potential_origin_squares &= disambiguation_rank;
        }

        let mut potential_moves = potential_origin_squares
            .into_iter()
            .map(|square| Move {
                from: square,
                to: destination_square,
                promotion,
            })
            .filter(|move_| game_state.is_legal_move(*move_));

        // If there is exactly one potential move return it. Else return ChessParsingError.
        if let Some(result) = potential_moves.next() {
            if potential_moves.next().is_none() {
                return Ok(result);
            }
        }
        Err(ChessParseError())
    }
}

impl Display for Move {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}{}",
            self.from,
            self.to,
            match self.promotion {
                Promotion::NONE => "",
                Promotion::KNIGHT => "n",
                Promotion::BISHOP => "b",
                Promotion::ROOK => "r",
                Promotion::QUEEN => "q",
            }
        )
    }
}

impl Display for MoveSequence {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use itertools::Itertools;

        write!(f, "{}", self.into_iter().map(|m| m.to_string()).join(" "))
    }
}

impl Display for MoveSet {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use itertools::Itertools;

        write!(f, "{}", self.into_iter().map(|m| m.to_string()).join(" "))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsing::FenConvertable;

    #[test]
    fn print_e2e4() {
        let move_ = Move {
            from: Square::E2,
            to: Square::E4,
            promotion: Promotion::NONE,
        };
        assert_eq!(move_.to_string(), "e2e4");
    }

    #[test]
    fn print_a1h8() {
        let move_ = Move {
            from: Square::A1,
            to: Square::H8,
            promotion: Promotion::NONE,
        };
        assert_eq!(move_.to_string(), "a1h8");
    }

    #[test]
    fn print_f7f8n() {
        let move_ = Move {
            from: Square::F7,
            to: Square::F8,
            promotion: Promotion::KNIGHT,
        };
        assert_eq!(move_.to_string(), "f7f8n");
    }

    #[test]
    fn parse_e2e4() {
        let move_ = Move {
            from: Square::E2,
            to: Square::E4,
            promotion: Promotion::NONE,
        };
        assert_eq!("e2e4".try_into(), Ok(move_));
    }

    #[test]
    fn parse_a1h8() {
        let move_ = Move {
            from: Square::A1,
            to: Square::H8,
            promotion: Promotion::NONE,
        };
        assert_eq!("a1h8".try_into(), Ok(move_));
    }

    #[test]
    fn parse_f7f8n() {
        let move_ = Move {
            from: Square::F7,
            to: Square::F8,
            promotion: Promotion::KNIGHT,
        };
        assert_eq!("f7f8n".try_into(), Ok(move_));
    }

    #[test]
    fn parse_invalid_square_f7f9() {
        let parsed_move: Result<Move, _> = "f7f9".try_into();
        assert_eq!(parsed_move, Err(ChessParseError()))
    }

    #[test]
    fn parse_invalid_square_a22b2() {
        let parsed_move: Result<Move, _> = "a22b2".try_into();
        assert_eq!(parsed_move, Err(ChessParseError()))
    }

    #[test]
    fn parse_invalid_square_f7f8c() {
        let parsed_move: Result<Move, _> = "f7f8c".try_into();
        assert_eq!(parsed_move, Err(ChessParseError()))
    }

    #[test]
    fn parse_invalid_square_a1b() {
        let parsed_move: Result<Move, _> = "a1b".try_into();
        assert_eq!(parsed_move, Err(ChessParseError()))
    }

    #[test]
    fn parse_san_e4() {
        let game_state = GameState::get_starting_position();
        let parsed_move = Move::from_san("e4", game_state).unwrap();
        let expected_move = Move {
            from: Square::E2,
            to: Square::E4,
            promotion: Promotion::NONE,
        };

        assert_eq!(parsed_move, expected_move)
    }

    #[test]
    fn parse_san_0_0() {
        let game_state = GameState::from_fen("r3k3/8/8/8/8/8/8/4K2R w K - 0 1").unwrap();
        let parsed_move = Move::from_san("0-0", game_state).unwrap();
        let expected_move = Move {
            from: Square::E1,
            to: Square::G1,
            promotion: Promotion::NONE,
        };

        assert_eq!(parsed_move, expected_move)
    }

    #[test]
    fn parse_invalid_san_ke2() {
        let game_state = GameState::get_starting_position();
        let parsed_move = Move::from_san("Ke2", game_state);

        assert_eq!(parsed_move, Err(ChessParseError()))
    }

    #[test]
    fn parse_san_ne3d5() {
        let game_state = GameState::from_fen("7k/2N1N3/8/8/8/2N1N3/8/4K3 w - - 0 1").unwrap();
        let parsed_move = Move::from_san("Ne3d5", game_state).unwrap();
        let expected_move = Move {
            from: Square::E3,
            to: Square::D5,
            promotion: Promotion::NONE,
        };

        assert_eq!(parsed_move, expected_move)
    }
}
