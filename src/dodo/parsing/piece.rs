// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

use std::convert::TryFrom;
use std::fmt::{self, Display, Formatter};

use crate::parsing::ChessParseError;
use crate::representation::{Color, Piece, PieceKind};

impl Piece {
    pub fn to_letter_symbol(self) -> char {
        match self {
            Piece {
                color: Color::WHITE,
                kind: PieceKind::PAWN,
            } => 'P',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::KNIGHT,
            } => 'N',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::BISHOP,
            } => 'B',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::ROOK,
            } => 'R',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::QUEEN,
            } => 'Q',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::KING,
            } => 'K',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::PAWN,
            } => 'p',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::KNIGHT,
            } => 'n',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::BISHOP,
            } => 'b',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::ROOK,
            } => 'r',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::QUEEN,
            } => 'q',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::KING,
            } => 'k',
        }
    }

    pub fn from_letter_symbol(symbol: char) -> Result<Piece, ChessParseError> {
        match symbol {
            'P' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::PAWN,
            }),
            'N' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::KNIGHT,
            }),
            'B' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::BISHOP,
            }),
            'R' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::ROOK,
            }),
            'Q' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::QUEEN,
            }),
            'K' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::KING,
            }),
            'p' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::PAWN,
            }),
            'n' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::KNIGHT,
            }),
            'b' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::BISHOP,
            }),
            'r' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::ROOK,
            }),
            'q' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::QUEEN,
            }),
            'k' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::KING,
            }),
            _ => Err(ChessParseError()),
        }
    }

    pub fn to_unicode_symbol(self) -> char {
        match self {
            Piece {
                color: Color::WHITE,
                kind: PieceKind::PAWN,
            } => '♙',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::KNIGHT,
            } => '♘',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::BISHOP,
            } => '♗',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::ROOK,
            } => '♖',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::QUEEN,
            } => '♕',
            Piece {
                color: Color::WHITE,
                kind: PieceKind::KING,
            } => '♔',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::PAWN,
            } => '♟',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::KNIGHT,
            } => '♞',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::BISHOP,
            } => '♝',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::ROOK,
            } => '♜',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::QUEEN,
            } => '♛',
            Piece {
                color: Color::BLACK,
                kind: PieceKind::KING,
            } => '♚',
        }
    }

    pub fn from_unicode_symbol(symbol: char) -> Result<Piece, ChessParseError> {
        match symbol {
            '♙' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::PAWN,
            }),
            '♘' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::KNIGHT,
            }),
            '♗' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::BISHOP,
            }),
            '♖' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::ROOK,
            }),
            '♕' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::QUEEN,
            }),
            '♔' => Ok(Piece {
                color: Color::WHITE,
                kind: PieceKind::KING,
            }),
            '♟' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::PAWN,
            }),
            '♞' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::KNIGHT,
            }),
            '♝' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::BISHOP,
            }),
            '♜' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::ROOK,
            }),
            '♛' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::QUEEN,
            }),
            '♚' => Ok(Piece {
                color: Color::BLACK,
                kind: PieceKind::KING,
            }),
            _ => Err(ChessParseError()),
        }
    }
}

impl TryFrom<char> for Piece {
    type Error = ChessParseError;

    fn try_from(symbol: char) -> Result<Piece, Self::Error> {
        Piece::from_letter_symbol(symbol)
    }
}

impl TryFrom<char> for PieceKind {
    type Error = ChessParseError;

    fn try_from(symbol: char) -> Result<PieceKind, Self::Error> {
        Piece::from_letter_symbol(symbol).map(|p| p.kind)
    }
}

impl From<Piece> for char {
    fn from(piece: Piece) -> char {
        piece.to_letter_symbol()
    }
}

impl Display for Piece {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", char::from(*self))
    }
}
