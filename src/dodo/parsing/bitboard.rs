// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

use std::fmt::{Debug, Display, Formatter, Result};

use crate::representation::square::strum::IntoEnumIterator;
use crate::representation::{Bitboard, File, Rank, Square};

impl Display for Bitboard {
    fn fmt(&self, f: &mut Formatter) -> Result {
        writeln!(f)?;
        writeln!(f, "   A B C D E F G H")?;
        writeln!(f, " ┌─────────────────┐")?;

        for rank in Rank::iter().rev() {
            write!(f, "{rank}│ ")?;
            for file in File::iter() {
                write!(f, "{} ", i32::from(self.is_set(Square::new(file, rank))))?;
            }
            writeln!(f, "│{rank}")?;
        }

        writeln!(f, " └─────────────────┘")?;
        writeln!(f, "   A B C D E F G H")?;

        Ok(())
    }
}

impl Debug for Bitboard {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{self}")
    }
}

#[cfg(test)]
mod tests {
    extern crate indoc;

    use indoc::indoc;

    use super::*;

    #[test]
    fn test_print_bitboard() {
        assert_eq!(
            Bitboard::from_squares(&[Square::A1, Square::A5, Square::E4, Square::H8]).to_string(),
            indoc!(
                "

                    A B C D E F G H
                  ┌─────────────────┐
                 8│ 0 0 0 0 0 0 0 1 │8
                 7│ 0 0 0 0 0 0 0 0 │7
                 6│ 0 0 0 0 0 0 0 0 │6
                 5│ 1 0 0 0 0 0 0 0 │5
                 4│ 0 0 0 0 1 0 0 0 │4
                 3│ 0 0 0 0 0 0 0 0 │3
                 2│ 0 0 0 0 0 0 0 0 │2
                 1│ 1 0 0 0 0 0 0 0 │1
                  └─────────────────┘
                    A B C D E F G H
                "
            )
        )
    }
}
