// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2019 Sven Stegemann

extern crate regex;

use std::convert::TryFrom;
use std::fmt::{self, Display, Formatter};
use std::str::FromStr;

use crate::parsing::ChessParseError;
use crate::representation::{File, Rank, Square};

impl TryFrom<char> for Rank {
    type Error = ChessParseError;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        match c {
            '1' => Ok(Rank::_1),
            '2' => Ok(Rank::_2),
            '3' => Ok(Rank::_3),
            '4' => Ok(Rank::_4),
            '5' => Ok(Rank::_5),
            '6' => Ok(Rank::_6),
            '7' => Ok(Rank::_7),
            '8' => Ok(Rank::_8),
            _ => Err(ChessParseError()),
        }
    }
}

impl From<Rank> for char {
    fn from(rank: Rank) -> char {
        match rank {
            Rank::_1 => '1',
            Rank::_2 => '2',
            Rank::_3 => '3',
            Rank::_4 => '4',
            Rank::_5 => '5',
            Rank::_6 => '6',
            Rank::_7 => '7',
            Rank::_8 => '8',
        }
    }
}

impl Display for Rank {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", char::from(*self))
    }
}

impl TryFrom<char> for File {
    type Error = ChessParseError;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        match c.to_ascii_lowercase() {
            'a' => Ok(File::A),
            'b' => Ok(File::B),
            'c' => Ok(File::C),
            'd' => Ok(File::D),
            'e' => Ok(File::E),
            'f' => Ok(File::F),
            'g' => Ok(File::G),
            'h' => Ok(File::H),
            _ => Err(ChessParseError()),
        }
    }
}

impl From<File> for char {
    fn from(file: File) -> char {
        match file {
            File::A => 'a',
            File::B => 'b',
            File::C => 'c',
            File::D => 'd',
            File::E => 'e',
            File::F => 'f',
            File::G => 'g',
            File::H => 'h',
        }
    }
}

impl Display for File {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", char::from(*self))
    }
}

impl TryFrom<&str> for Square {
    type Error = ChessParseError;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let mut chars = s.chars();

        let file = chars.next().ok_or(ChessParseError())?.try_into()?;
        let rank = chars.next().ok_or(ChessParseError())?.try_into()?;

        if chars.count() > 0 {
            return Err(ChessParseError());
        }

        Ok(Square::new(file, rank))
    }
}

impl FromStr for Square {
    type Err = ChessParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Square::try_from(s)
    }
}

impl Display for Square {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}{}", self.get_file(), self.get_rank())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_lowercase_square() {
        assert_eq!(Square::try_from("b3"), Ok(Square::B3));
    }

    #[test]
    fn parse_uppercase_square() {
        assert_eq!(Square::try_from("G8"), Ok(Square::G8));
    }

    #[test]
    fn reject_parse_empty_string() {
        assert_eq!(Square::try_from(""), Err(ChessParseError()));
    }

    #[test]
    fn reject_parse_invalid_characters() {
        assert_eq!(Square::try_from("z9"), Err(ChessParseError()));
    }

    #[test]
    fn print_square() {
        assert_eq!(Square::G8.to_string(), "g8");
    }
}
