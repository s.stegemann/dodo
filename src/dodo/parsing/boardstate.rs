// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

use std::fmt::{Debug, Display, Formatter, Result};

use crate::representation::square::strum::IntoEnumIterator;
use crate::representation::{BoardState, File, Rank, Square};

impl Display for BoardState {
    fn fmt(&self, f: &mut Formatter) -> Result {
        writeln!(f)?;
        writeln!(f, "   A B C D E F G H")?;
        writeln!(f, " ┌─────────────────┐")?;

        for rank in Rank::iter().rev() {
            write!(f, "{rank}│ ")?;
            for file in File::iter() {
                write!(
                    f,
                    "{} ",
                    self.get_piece_on_square(Square::new(file, rank))
                        .map(|piece| piece.into())
                        .unwrap_or(' '),
                )?;
            }
            writeln!(f, "│{rank}")?;
        }

        writeln!(f, " └─────────────────┘")?;
        writeln!(f, "   A B C D E F G H")?;

        Ok(())
    }
}

impl Debug for BoardState {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{self}")
    }
}

#[cfg(test)]
mod tests {
    extern crate indoc;
    use indoc::indoc;

    use crate::representation::GameState;

    #[test]
    fn test_print_boardstate() {
        assert_eq!(
            GameState::get_starting_position().get_board().to_string(),
            indoc!(
                "

                   A B C D E F G H
                 ┌─────────────────┐
                8│ r n b q k b n r │8
                7│ p p p p p p p p │7
                6│                 │6
                5│                 │5
                4│                 │4
                3│                 │3
                2│ P P P P P P P P │2
                1│ R N B Q K B N R │1
                 └─────────────────┘
                   A B C D E F G H
                "
            )
        )
    }
}
