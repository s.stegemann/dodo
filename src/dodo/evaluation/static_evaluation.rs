// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

use crate::evaluation::Score;
use crate::representation::{Bitboard, GameState, Piece, PieceKind};
use crate::search::attacks::Attacks;

use enum_map::{enum_map, EnumMap};
use strum::IntoEnumIterator;

lazy_static! {
    pub static ref MATERIAL_VALUES: EnumMap<PieceKind, i32> = enum_map! {
        PieceKind::PAWN   => 100,
        PieceKind::KNIGHT => 325,
        PieceKind::BISHOP => 350,
        PieceKind::ROOK   => 525,
        PieceKind::QUEEN  => 950,
        PieceKind::KING   => 0,
    };
}

pub fn evaluate_statically(game_state: &GameState) -> Score {
    let board = game_state.get_board();
    let side_to_move = game_state.get_side_to_move();
    let other_side = side_to_move.get_opponent();
    let count_pieces = |piece_kind, color| {
        board
            .get_positions(Piece::new(color, piece_kind))
            .popcount() as i32
    };

    let mut centipawns = 0;

    for piece_kind in PieceKind::iter() {
        centipawns += MATERIAL_VALUES[piece_kind]
            * (count_pieces(piece_kind, side_to_move) - count_pieces(piece_kind, other_side))
    }

    Score::Centipawns(centipawns)
}

fn is_insufficient_material(game_state: &GameState) -> bool {
    let board = game_state.get_board();

    match board.get_occupied().popcount() {
        ..2 => unreachable!("There always has to be one king of each color."),
        2 => true,
        3 => {
            board.get_occupied_by_piece_kind(PieceKind::KNIGHT) != Bitboard::EMPTY
                || board.get_occupied_by_piece_kind(PieceKind::BISHOP) != Bitboard::EMPTY
        }
        4.. => false,
    }
}

fn last_move_repeated_position(history: &[GameState]) -> bool {
    let newest_game_state = history.last().expect("history must not be empty");

    for game_state in history
        .iter()
        .rev() // Iterate from newest to oldest game state.
        .step_by(2) // Ignore positions with the other side to move.
        .skip(1) // Skip the newest game state, because it always equals current_game_state.
        .skip(1) // Skip the game state 2 plies ago, because it never equals to current one.
        .take(newest_game_state.get_fifty_move_counter() as usize)
    // Ignore positions before the last irreversible move.
    {
        if game_state == newest_game_state {
            return true;
        }
    }

    false
}

pub fn evaluate_interior_node(history: &[GameState]) -> Option<Score> {
    let game_state = history.last().expect("history must not be empty.");

    if game_state.king_can_be_captured() {
        Some(Score::Max)
    } else if is_insufficient_material(game_state) || last_move_repeated_position(history) {
        Some(Score::Centipawns(0))
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn detect_repetion_as_draw() {
        let mut game_state = GameState::get_starting_position();
        let mut history = vec![game_state.clone()];

        for move_ in ["g1f3", "g8f6", "f3g1", "f6g8"] {
            let move_ = move_.try_into().unwrap();
            game_state.make_move(move_);
            history.push(game_state.clone());
        }

        assert!(last_move_repeated_position(&history))
    }
}
