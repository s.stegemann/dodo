// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2021 Sven Stegemann

extern crate derive_more;
extern crate strum_macros;

use std::cmp::Ordering;

use strum_macros::EnumDiscriminants;

use crate::util::plies_to_moves;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, EnumDiscriminants)]
#[strum_discriminants(derive(Ord, PartialOrd))] // Add .cmp() to ScoreDiscriminant.
#[strum_discriminants(vis(pub(self)))] // The derived ScoreDiscriminant should only be visible inside this module.
pub enum Score {
    Min,
    GettingMatedIn(i32),
    TablebaseLossIn(i32),
    Centipawns(i32),
    TablebaseWinIn(i32),
    MatingIn(i32),
    Max,
}

use Score::*; // shorten the use of enum variants in this file;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum ScoreRange {
    UpperBound(Score),
    Exact(Score),
    LowerBound(Score),
}

impl Score {
    pub fn search_score_to_tt_score(self, depth_from_root: i64) -> Self {
        // During search we use "moves from root" in scores, but
        // we have to save "moves from current position" in the tablebase,
        // because we might use it later with a different root position.
        let delta = plies_to_moves(depth_from_root);

        match self {
            GettingMatedIn(moves) => GettingMatedIn(moves - delta),
            TablebaseLossIn(moves) => TablebaseLossIn(moves - delta),
            TablebaseWinIn(moves) => TablebaseWinIn(moves - delta),
            MatingIn(moves) => MatingIn(moves - delta),
            _ => self,
        }
    }

    pub fn tt_score_to_search_score(self, depth_from_root: i64) -> Self {
        // During search we use "moves from root" in scores, but
        // we have to save "moves from current position" in the tablebase,
        // because we might use it later with a different root position.
        let delta = plies_to_moves(depth_from_root);

        match self {
            GettingMatedIn(moves) => GettingMatedIn(moves + delta),
            TablebaseLossIn(moves) => TablebaseLossIn(moves + delta),
            TablebaseWinIn(moves) => TablebaseWinIn(moves + delta),
            MatingIn(moves) => MatingIn(moves + delta),
            _ => self,
        }
    }
}

impl std::ops::Neg for Score {
    type Output = Self;

    fn neg(self) -> Self {
        match self {
            Min => Max,
            GettingMatedIn(moves) => MatingIn(moves),
            TablebaseLossIn(moves) => TablebaseWinIn(moves),
            Centipawns(centipawns) => Centipawns(-centipawns),
            TablebaseWinIn(moves) => TablebaseLossIn(moves),
            MatingIn(moves) => GettingMatedIn(moves),
            Max => Min,
        }
    }
}

impl PartialOrd for Score {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Score {
    fn cmp(&self, other: &Self) -> Ordering {
        let self_discriminant: ScoreDiscriminants = self.into();
        let other_discriminant: ScoreDiscriminants = other.into();

        if self_discriminant != other_discriminant {
            // For different kinds of scores just sort from GettingMatedIn to MatingIn.
            self_discriminant.cmp(&other_discriminant)
        } else {
            // For white wins a lower inner value is better (faster mate).
            // For black wins a higher inner value is better (slower mate).
            // For centipawn evaluations higher is better.
            match (*self, *other) {
                (GettingMatedIn(left_moves), GettingMatedIn(right_moves)) => {
                    left_moves.cmp(&right_moves)
                }
                (TablebaseLossIn(left_moves), TablebaseLossIn(right_moves)) => {
                    left_moves.cmp(&right_moves)
                }
                (Centipawns(left_moves), Centipawns(right_moves)) => left_moves.cmp(&right_moves),
                (TablebaseWinIn(left_moves), TablebaseWinIn(right_moves)) => {
                    right_moves.cmp(&left_moves)
                }
                (MatingIn(left_moves), MatingIn(right_moves)) => right_moves.cmp(&left_moves),
                _ => Ordering::Equal,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_white_mates_vs_centipawns() {
        assert!(MatingIn(3) > Centipawns(29))
    }

    #[test]
    fn test_black_mates_vs_black_mates() {
        assert!(GettingMatedIn(10) > GettingMatedIn(5))
    }

    #[test]
    fn test_white_reaches_tablebase_win_vs_white_reaches_tablebase_win() {
        assert!(TablebaseWinIn(10) < TablebaseWinIn(5))
    }
}
