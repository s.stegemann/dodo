// This file is part of dodo.
//
// dodo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dodo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dodo.  If not, see <http://www.gnu.org/licenses/>.

// Copyright © 2022 Sven Stegemann

/// Converts a distance in the search tree into a number of full moves.
///
/// This is useful for example for converting "depth from root" to "mating in N moves".
pub fn plies_to_moves<T: Into<i64>>(plies: T) -> i32 {
    ((plies.into() + 1) / 2) as i32
}

#[cfg(not(feature = "hardware_pext"))]
mod pext {
    const N_BITS: usize = 6;

    #[repr(C)]
    struct zp7_masks_64_t {
        mask: u64,
        ppp_bits: [u64; N_BITS],
    }

    extern "C" {
        // fn zp7_pext_64(a: u64, mask: u64) -> u64;
        fn zp7_ppp_64(mask: u64) -> zp7_masks_64_t;
        fn zp7_pext_pre_64(a: u64, masks: *const zp7_masks_64_t) -> u64;
    }

    pub struct PextMask(zp7_masks_64_t);

    /// Extract bits on from every position where mask has a 1 bit into the least-significant bits of the result.
    impl PextMask {
        pub fn new(mask: u64) -> PextMask {
            unsafe { PextMask(zp7_ppp_64(mask)) }
        }

        /// Doc test
        pub fn apply(self, value: u64) -> u64 {
            unsafe { zp7_pext_pre_64(value, &self.0) }
        }
    }
}

#[cfg(feature = "hardware_pext")]
mod pext {
    extern crate bitintr;

    /// Extract bits on from every position where mask has a 1 bit into the least-significant bits of the result.
    pub struct PextMask(u64);

    impl PextMask {
        pub fn new(mask: u64) -> PextMask {
            PextMask(mask)
        }

        pub fn apply(self, value: u64) -> u64 {
            use bitintr::*;

            value.pext(self.0)
        }
    }
}

pub use pext::PextMask;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pext() {
        let mask = PextMask::new(0xff00fff0f000f0f0u64);
        let value = 0x0123456789abcdefu64;
        let expected_result = 0x014568ceu64;

        assert_eq!(mask.apply(value), expected_result);
    }
}
