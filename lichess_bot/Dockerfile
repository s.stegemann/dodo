# Using python 3.9 breaks the dependency on PyYAML.
ARG PYTHON_BUILDER_IMAGE=python:3.8-bullseye 
ARG RUST_BUILDER_IMAGE=rustlang/rust:nightly
ARG RUNTIME_IMAGE=python:3.8-slim-bullseye

FROM $PYTHON_BUILDER_IMAGE as lichess_bot_builder
ARG LICHESS_BOT_VERSION=1.1.3

RUN wget -O - https://github.com/ShailChoksi/lichess-bot/archive/refs/tags/$LICHESS_BOT_VERSION.tar.gz \
  | tar -xz \
  && mv lichess-bot-$LICHESS_BOT_VERSION /opt/lichess_bot

WORKDIR /opt/lichess_bot

ENV LANG=C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PATH="/opt/venv/bin:$PATH"
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

RUN python3 -m venv /opt/venv --copies \
 && python3 -m pip install wheel \
 && python3 -m pip install --no-cache-dir -r requirements.txt

FROM $RUST_BUILDER_IMAGE as dodo_builder
COPY . .
# TODO: evaluate if we can speed this up with cargo-chef.
RUN cargo build --verbose --release

FROM $RUNTIME_IMAGE
MAINTAINER Sven Stegemann
WORKDIR /opt/lichess_bot

RUN groupadd -g 999 lichess_bot_user && \
    useradd -r -u 999 -g lichess_bot_user lichess_bot_user
USER lichess_bot_user

ENV PYTHONDONTWRITEBYTECODE=1
ENV PATH="/opt/venv/bin:$PATH"

COPY --from=lichess_bot_builder /opt/lichess_bot /opt/lichess_bot
COPY --from=lichess_bot_builder /opt/venv /opt/venv
COPY --from=dodo_builder target/release/dodo lichess_bot/engines
COPY lichess_bot/config.yml /opt/lichess_bot 
COPY lichess_bot/Perfect2021.bin /opt/lichess_bot/engines
ENTRYPOINT ["/opt/venv/bin/python3", "/opt/lichess_bot/lichess-bot.py"]
# ENTRYPOINT bash
