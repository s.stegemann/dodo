Dodo
====

**Dodo is a small chess engine I am building as a training project.**

---

Usage
-----

This crate will generate a UCI compatible binary and a library. You can use Dodo in the following ways:

* Play against Dodo or use Dodo for analyzing positions using a UCI compatible chess GUI.
* Play against the Dodo lichess bot. (Coming soon)
* Run the Dodo binary on a command line terminal and control it using UCI commands.
* Include the library in a Rust project.

Roadmap
-------

This project is still in an early stage of development. The first milestone is to create a simple engine, that can be used in a chess GUI. At the moment I am not optimizing any code or implementing advanced features to increase its playing strength. To get a working engine the following parts have to be implemented:

* [x] Modelling the basic chess entities like pieces, moves, positions etc.
* [x] Generating a list of possible moves for a given position.
* [x] Parsing the different entities from strings and printing them to strings.
* [x] An UCI interface to allow communication with GUIs.
* [x] An evaluation function which heuristically tries to determine how advantageous a position is.
* [x] A search algorithm which uses the move generator and evaluation function to determine the best move.

After that the next steps will be

* [x] Cleaning up the attack and move generation code.
* [ ] Improving the documentation and comments.
* [ ] Adding an extensive test suite to ensure that following optimizations don't break correctness.

Documentation
-------------

The following resources might be useful for understanding or extending Dodo.

* `cargo doc --open` will generate and show a reference level documentation for the dodo library.
* The [specification of the Universal Chess Interface (UCI)](docs/engine-interface.txt).
  Dodo uses the UCI to talk to different chess GUIs.
* The [Chessprogramming Wiki](https://www.chessprogramming.org/Main_Page) is a great resource for many of the concepts used in Dodo.

License
-------

Dodo is released as free software under the terms of the [GPL v3](LICENSE).
